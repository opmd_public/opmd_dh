#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  7 11:40:35 2023

@author: weatherill
"""

from OPMD_dh.file_formats.hdf5 import H5OPMDFile, OPMDDataTypes
import numpy as np

dat = np.random.normal(loc=200.0, scale=10.0, size=(200,200)).astype(np.uint16)


fl = H5OPMDFile("testdat")

fl.add_data("testarr", dat)


fl.mutable_metadata["metatest_BLAH"]=" longstringseeifthisworks"

harr = fl.data


dat2 = np.random.normal(size=(400,400))
dat = fl.add_data("testarr2", dat2)
dat.mutable_metadata["metatest1"] = 12

tbl1 = {k : k**2 for k in range(100)}

columns = [list(_) for _ in (tbl1.keys(), tbl1.values())]

rarr = np.rec.fromarrays(columns, names=["k", "k**2"])

fl.add_data("testtbl", rarr)


fl.save("testout.H5")