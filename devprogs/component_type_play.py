import sqlalchemy as sa
from OPMD_dh.metadata.dbmodel import BaseModel, ComponentType
from sqlalchemy.orm import sessionmaker
from OPMD_dh.metadata.schemas import OperatorSchema, ComponentTypeSchema, ComponentSchema, LocationSchema
from OPMD_dh.metadata.dbmodel import ComponentType, Component, Location, Operator
from OPMD_dh.datahandler import DHTool

REPODIR: str = "~/Documents/dh_expts/test_repo"
engine = sa.create_engine("sqlite:///:memory:")
BaseModel.metadata.create_all(engine)

Session = sessionmaker(bind=engine)
handler = DHTool.from_path(REPODIR, sqlalchemy_engine=engine, sessiontp=Session)


with Session() as sess:
    op = Operator(name="Dan Weatherill", email="daniel.weatherill@physics.ox.ac.uk",
                  institution="University of Oxford")
    
    oxford_loc = Location(descriptive_name="Oxford OPMD")

    edmund = ComponentType(descriptive_name="Techspec Edmund 35mm lens",
                           manufacturer="Edmund Optics",
                           manufacturer_part_number="#86-573")

    lens1 = Component(project_serial_number="201",
                      manufacturer_serial_number="1209382",
                      notes="nonexistent test lens",
                      current_location=oxford_loc,
                      type=edmund,
                      entered_by=op)

    sess.add(oxford_loc)
    sess.add(edmund)

    sess.add(lens1)
    sess.commit()

    cdat = ComponentSchema(many=False).dump(lens1)

    print(oxford_loc.get_dumpfilepath(handler))
    print(lens1.get_dumpfilepath(handler))

print(ops)
print(edmund)
print(lens1)

handler.dump_standalone_schema_to_file(ComponentTypeSchema, overwrite=True)



print(cdat)
