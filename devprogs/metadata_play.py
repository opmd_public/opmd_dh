from OPMD_dh.schemas.experiments import Experiment, Operator, ExperimentalProcedure
from OPMD_dh.schemas.data import DatasetType, DatasetClass, Dataset, ArtifactType, StorageType, Artifact
from OPMD_dh.schemas.metadata import MetadataItem, MetadataItemDescriptor
from OPMD_dh.file_formats.netcdf import NetCDFImage

import os
import pwd

if __name__ == "__main__":
    fullname = pwd.getpwuid(os.getuid())[4]
    op = Operator(name=fullname, email="@",
                  institution="Oxford")

    mdi = MetadataItemDescriptor

    atype=ArtifactType(name="Flat image",
                       afact_metadata_desc=tuple([mdi(name="etime", unit="s", python_type=float, comment="exposure time in seconds")]))

    stype = StorageType(name="netCDF4 Image",
                        read_write_class=NetCDFImage)

    print(atype.__hash__())
    print(stype.__hash__())

    n_frames = MetadataItemDescriptor(name="n_frames",
                                      python_type=int,
                                      unit=None,
                                      comment="number of frames")

    dset_type = DatasetType(name="superflat",
                            storage_desc={atype: stype},
                            dset_class=DatasetClass.EXPERIMENTAL,
                            dset_metadata_desc=frozenset([n_frames]))

    afacts = [Artifact(f"/{n}.H5", stype, atype, None) for n in range(10)]


