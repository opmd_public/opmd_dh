from OPMD_dh.procedures import ExperimentalProcedure
from OPMD_dh.metadata.dbmodel import ProcedureClass, ComponentType, Component, Operator, ArtifactType
from OPMD_dh.datahandler import DHTool
from OPMD_dh.operator_utils import ConfigManager
import logging
import numpy as np 
import sqlalchemy as sa

import pint
ureg = pint.UnitRegistry()



logging.basicConfig(level=logging.DEBUG)


class ExampleProceduresBase(ExperimentalProcedure):
    def setup_dut(self, **kwargs):
        with self._dhtool.Session() as sess:
            ctype = ComponentType.load_or_create(sess, lookup_kw="descriptive_name",
                                                 descriptive_name="Test Component",
                                                 manufacturer="Test Company")

            comp = Component.load_or_create(sess, lookup_kw="project_serial_number",
                                            project_serial_number="TEST001",
                                            entered_by=self.operator,
                                            type=ctype)

        return comp

    def setup_hw(self, *args, **kwargs):
        pass

    def extract_artifact_metadata(self, afact):
        return getattr(afact, "_mdat", {})
    def finalize_artifact(self, path, afact, mdat):
        print(f"saving to path {path}")


class ExampleDarkProcedure(ExampleProceduresBase):
    ONEWORD_NAME="ExampleDark"
    FRIENDLY_NAME = "Example Dark Procedure to test db stuff"
    PROCEDURE_CLASS = ProcedureClass.EXPERIMENT

    def identify_artifact_type(self, afact, dbsess):
        fpathfmt = "ExampleDark-{seqnum}_{dt:%Y-%m-%d_%H-%M-%S}.npy"
        atp = ArtifactType.load_or_create(dbsess, lookup_kw="name",
                                          name="ExampleNumpyDarkArtifactType",
                                          file_path_format=fpathfmt,
                                          comment="numpy artifact from example tests"
                                          )
        return atp


    def plan(self, nframes: int, exposure_time: ureg.Quantity):
        return [{}]

    def run_step(self,  nframes, exposure_time, **kwargs):
        for i in range(nframes):
            data = np.random.poisson(exposure_time.to(ureg.us).magnitude, size=[100,100])
            yield data

    def __init__(self, dhtool, nframes: int, exposure_time: ureg.Quantity, **kwargs):
        kwargs = self.pack_argument_helper()
        print(kwargs)
        super().__init__(**kwargs)


class ExamplePTCProcedure(ExampleProceduresBase):
    ONEWORD_NAME="ExamplePTC"
    FRIENDLY_NAME="Example TPC procedure to test db stuff"

    def __init__(self, dhtool, nframes_per_step: int, exposure_time_min: ureg.Quantity,
                 exposure_time_max: ureg.Quantity, exposure_time_steps: int, **kwargs):
        kwargs = self.pack_argument_helper()
        print(kwargs)
        super().__init__(**kwargs)

    def plan(self, exposure_time_min: ureg.Quantity, exposure_time_max: ureg.Quantity, exposure_time_steps: int, **kwargs):
        etimes = np.linspace(exposure_time_min,  exposure_time_max,
                             exposure_time_steps)
        for time in etimes:
            yield {"exposure_time" : time, "etime_display" : int(time.to(ureg.us).magnitude)}

    def run_step(self, exposure_time, nframes_per_step, **kwargs):
        for i in range(nframes_per_step):
            data = np.random.poisson(exposure_time.to(ureg.us).magnitude, size=[100,100])
            yield data

    def identify_artifact_type(self, afact, dbsess):
        fpathfmt = "ExampleFlat-{step.plan_data[etime_display]}-{seqnum}_{dt:%Y-%m-%d_%H-%M-%S}.npy"
        atp = ArtifactType.load_or_create(dbsess, lookup_kw="name",
                                          name="ExampleNumpyPTCArtifactType",
                                          file_path_format=fpathfmt,
                                          comment="numpy artifact from example tests",
                                          overwrite=True
                                          )
        return atp

if __name__ == "__main__":
    default_repo_path = ConfigManager.get_default_repo()
    print(default_repo_path)    
    dhtool = DHTool.from_dbfile(default_repo_path,
                                "/local/scratch/dh_test/test.sqlite")

    proc = ExampleDarkProcedure(dhtool, nframes=50, exposure_time=30.0*ureg.us,
                                loglevel=logging.DEBUG)

    proc.run()

    proc2 = ExamplePTCProcedure(dhtool, nframes_per_step =2,
                                exposure_time_min = 0.02*ureg.ms,
                                exposure_time_max = 200 * ureg.us,
                                exposure_time_steps = 30)

    proc2.run()

