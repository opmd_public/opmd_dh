from OPMD_dh.metadata.dbmodel import BaseModel, Operator, Location, ComponentType, Component, ProcedureType, ProcedureClass
from OPMD_dh.metadata.schemas import OperatorSchema, LocationSchema, ComponentTypeSchema, ComponentSchema, ProcedureTypeSchema
from OPMD_dh.datahandler import DHTool
from OPMD_dh.operator_utils import OperatorManager
from sqlalchemy import create_engine, select
from sqlalchemy.orm import sessionmaker

#this script manually creates the components that are needed for the lens testing
engine = create_engine("sqlite:///:memory:")
BaseModel.metadata.create_all(engine)
Session = sessionmaker(bind=engine)

tool = DHTool.from_path("/home/danw/Documents/magis_testdata", sqlalchemy_engine=engine,
                        sessiontp=Session)

with Session() as sess:
    tool.load_all_schemas(sess)
    op = OperatorManager.get_operator(tool, sess)
    print(op.name)

    stmt = select(ComponentType).where(ComponentType.id == 1)
    camcomp = sess.execute(stmt).scalars().one()

    stmt = select(ComponentType).where(ComponentType.id == 2)
    lenscomp = sess.execute(stmt).scalars().one()

    otf_data_acq_tp = ProcedureType(name="Pupil Measurement (Legacy)",
                                    notes="Simai Jia's project data",
                                    procclass = ProcedureClass.EXPERIMENT,
                                    component_types = [lenscomp, camcomp])

    sess.add(otf_data_acq_tp)

    sess.commit()

    tool.dump_all_schemas(sess)

    
    # loc = Location(descriptive_name="OPMD Lab (lens test bench)",
    #                lat=51.759013,
    #                lon=-1.256269)

    # sess.add(loc)
    # ueyecam_lenstest_type = ComponentType(descriptive_name="IDS uEye UI348xCP-M",
    #                                       manufacturer="IDS Imaging",
    #                                       manufacturer_part_number="UI348xCP-M")


    # sciencelens_type = ComponentType(descriptive_name="Edmund Tecspec high resolution 35mm",
    #                                  manufacturer="Edmund Optics",
    #                                  manufacturer_part_number="#86-573")

    # sess.add(ueyecam_lenstest_type)
    # sess.add(sciencelens_type)

    # ueyecam_lenstest = Component(manufacturer_serial_number="4103584201",
    #                              type=ueyecam_lenstest_type,
    #                              notes="Camera used on lens testing bench for OTF/pupil function (Simai Jia's project and follow up)",
    #                              current_location=loc,
    #                              entered_by=op,
    #                              project_serial_number="151")

    # sciencelens_one = Component(manufacturer_serial_number="UNKNOWN",
    #                             type=sciencelens_type,
    #                             notes="First Science Lens purchased",
    #                             current_location=loc,
    #                             entered_by=op,
    #                             project_serial_number="201")

    # sess.add(ueyecam_lenstest)
    # sess.commit()

    # tool.dump_all_schemas(sess)
