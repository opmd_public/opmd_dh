import sqlalchemy as sa
from OPMD_dh.metadata.dbmodel import BaseModel, Operator
from sqlalchemy.orm import sessionmaker
from OPMD_dh.metadata.schemas import OperatorSchema
from OPMD_dh.datahandler import DHTool
import os
import pathlib
import yaml
from OPMD_dh import Loader, Dumper

REPODIR = "~/Documents/dh_expts/test_repo"

engine = sa.create_engine("sqlite:///:memory:")
BaseModel.metadata.create_all(engine)

Session = sessionmaker(bind=engine)

handler = DHTool.from_path(REPODIR, sqlalchemy_engine=engine, sessiontp=Session)

with Session() as sess:
    ops = Operator(name="Dan Weatherill",
                   email="daniel.weatherill@physics.ox.ac.uk",
                   )
    sess.add(ops)
    sess.commit()

with Session() as sess:
    handler.dump_standalone_schema_to_file(OperatorSchema, overwrite=True)

with Session() as sess:
    allops = sess.query(Operator).all()
    for op in allops:
        print(op.name)
        print(op.id)

print(allops)
