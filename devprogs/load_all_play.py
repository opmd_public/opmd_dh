from OPMD_dh.datahandler import DHTool
from OPMD_dh.metadata.dbmodel import BaseModel, Operator
import sqlalchemy as sa
from sqlalchemy.orm import sessionmaker, query

REPODIR: str = "~/Documents/dh_expts/test_repo"
engine = sa.create_engine("sqlite:///:memory:")
BaseModel.metadata.create_all(engine)

Session = sessionmaker(bind=engine)
tool = DHTool.from_path(REPODIR, sqlalchemy_engine=engine,
                        sessiontp=Session)

tool.load_all_schemas()

stmt = sa.select(Operator).order_by(Operator.id)
with Session() as sess:
    ops = sess.execute(stmt).scalars().all()

    for op in ops:
        print(op.name)
