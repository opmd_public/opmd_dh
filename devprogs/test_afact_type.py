import logging
logging.basicConfig(level=logging.DEBUG)

from OPMD_dh.artifacts import ArtifactBase
from OPMD_dh.procedures import ProcedureBase
from OPMD_dh.file_formats.hdf5 import H5OPMDFile
from OPMD_dh.datahandler import DHTool
from OPMD_dh.metadata.dbmodel import ComponentType, Component, Operator
import sqlalchemy as sa




class TestDarkArtifact(ArtifactBase):
    STORAGE_CLASS = H5OPMDFile
    ARTIFACT_TYPE_NAME = "TEST_DARK_IMAGES"
    FILE_PATH_FORMAT = "testdark_{seqnum}_{dt.isoformat()}.H5"


class TestDarkCurrentProcedure(ProcedureBase):
    BASE_PATH_FORMAT: str = "{procedure.device_under_test.project_serial_number}/testdark/{procedure.uuid}"
    ONEWORD_NAME: str = "testdark"
    FRIENDLY_NAME: str = "Dark Current Procedure (testing version)"

    def setup_dut(self):
        with self._dhtool._ensuresession() as sess:
            oper = Operator.load_or_create(sess, name="test user",
                                           email="test@test.com",
                                           lookup_kw="email")

            ctype = ComponentType.load_or_create(sess, manufacturer="Nobody",
                                                 descriptive_name="test component (Dummy)",
                                                 lookup_kw="descriptive_name")

            comp = Component.load_or_create(sess, project_serial_number="TEST01A",
                                            type=ctype, entered_by=oper)

            sess.commit()


        return comp

DBPATH = "/scratch/test.sqlite"
tool = DHTool.from_dbfile(None, DBPATH)

from sqlalchemy.inspection import inspect

pkey = inspect(Component).primary_key
print(pkey)



proc = TestDarkCurrentProcedure(dhtool=tool)

print(proc._procobj.artifact_path)

# print(proc._proctypeobj.base_file_path)
# print(proc._proctypeobj.class_name)
# print(__file__)

# print(proc._procobj.uuid)
# print(proc._procobj._seqnum)
# print(proc._procobj.create_time)
