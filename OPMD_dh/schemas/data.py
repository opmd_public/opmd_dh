from marshmallow_dataclass import dataclass
from enum import Enum
from typing import List, Any, Type, Dict, Tuple, FrozenSet
from .metadata import MetadataItemDescriptor, MetadataItem


class DatasetClass(Enum):
    EXPERIMENTAL = "Experimental"
    DERIVED = "Derived"
    CALIBRATION = "Calibration"
    OTHER = "Other"


@dataclass(eq=True, frozen=True)
class ArtifactType:
    """Schema which describes the type of a data artifact. Should include information such as image type (i.e. flat, spot etc)"""
    name: str
    afact_metadata_desc: FrozenSet[MetadataItemDescriptor]

@dataclass(eq=True, frozen=True)
class StorageType:
    name: str
    read_write_class: Type

@dataclass(eq=True, frozen=True)
class DatasetType:
    """Schema which describes the definition of a dataset"""
    name: str
    dset_class: DatasetClass
    storage_desc: Dict[ArtifactType, StorageType]
    dset_metadata_desc: FrozenSet[MetadataItemDescriptor]

@dataclass
class Artifact:
    """Schema for a specific data artifact"""
    path: str
    storage: StorageType
    artifact_type: ArtifactType
    afact_metadata: List[MetadataItem]

@dataclass
class Dataset:
    """Schema for a specific dataset"""
    dataset_type: DatasetType
    artifacts: Tuple[Artifact]
    dset_metadata: List[MetadataItem]
