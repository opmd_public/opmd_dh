from marshmallow_dataclass import dataclass
from typing import Dict, Any, Tuple, Type, List, Optional
from datetime import datetime
from enum import Enum

from .component import Component, ComponentType
from .data import DatasetType, Dataset
from .metadata import MetadataItem, MetadataItemDescriptor

_include_in_statepoint = {"statepoint" : True}

class ExperimentStatus(Enum):
    TODO = 0
    RUNNING = 1
    COMPLETE = 2
    ERROR = 3

class AnalysisStatus(Enum):
    TODO = 0
    COMPLETE = 2
    ERROR = 3

@dataclass
class Operator:
    """Schema for the operator who took an experimental dataset"""
    name: str
    email: str
    institution: str

@dataclass
class ProcedureDescriptor:
    """Schema for defining a generic workflow procedure (experiment or analysis)"""
    name: str
    post_metadata_schema: List[MetadataItemDescriptor]
    pre_metadata_schema: List[MetadataItemDescriptor]
    component_types: List[ComponentType]
    notes: str

@dataclass
class AnalysisProcedure(ProcedureDescriptor):
    input_types: List[ProcedureDescriptor]
    dataset_type: DatasetType

@dataclass
class ExperimentalProcedure(ProcedureDescriptor):
    """Schema for defining an experimental procedure that produces a dataset"""
    dataset_type: DatasetType


@dataclass
class Procedure:
    """Schema describing a specific workflow procedure (experiment or analysis)"""
    procedure: ProcedureDescriptor
    operator: Operator
    time_started: datetime
    notes: str
    pre_metadata: List[MetadataItem]
    post_metadata: List[MetadataItem]
    components: List[Component]
    code_description: str
    code_version: str
    code_patch: str
    dataset: Dataset

    
class Experiment(Procedure):
    """Schema describing a specific experiment produced by an ExperimentalProcedure"""
    time_ended: Optional[datetime]
    status: ExperimentStatus

class Analysis(Procedure):
    """Schema describing a specific analysis procedure"""
    status: AnalysisStatus
