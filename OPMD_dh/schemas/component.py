from dataclasses import dataclass
from typing import Union, Optional

IntOrStr = Union[int, str]

@dataclass
class ComponentType:
    """Schema to describe a generic hardware component"""
    description: str
    manufacturer_part_number: Optional[IntOrStr]

@dataclass
class Component:
    """Schema to describe a specific instance of a hardware component"""
    type: ComponentType
    manufacturer_serial_number: Optional[IntOrStr]
    project_serial_number: Optional[IntOrStr]
