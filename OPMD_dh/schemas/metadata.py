from marshmallow_dataclass import dataclass
from typing import Type, Any, Optional, List

@dataclass(eq=True, frozen=True)
class MetadataItemDescriptor:
    """schema for a metadata item that should appear """
    name: str
    unit: Optional[str]
    python_type: str
    comment: str

@dataclass
class MetadataItem:
    """A representation of an individual metadata item"""
    desc: MetadataItemDescriptor
    value: Any
