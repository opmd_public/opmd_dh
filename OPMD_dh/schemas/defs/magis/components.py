from OPMD_dh.schemas.component import ComponentType

lucid_camera = ComponentType(description="Lucid Vision TRI204S-M",
                             manufacturer_part_number="TRI204S-M")

science_lens = ComponentType(description="Edmund Tecspec 35mm #86-573",
                             manufacturer_part_number="#86-573")
