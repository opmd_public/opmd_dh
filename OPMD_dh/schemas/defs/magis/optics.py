from OPMD_dh.schemas.metadata import MetadataItemDescriptor
from OPMD_dh.schemas.data import DatasetType, DatasetClass, ArtifactType
from OPMD_dh.schemas.defs.camera_generic import exposure_time_s
from OPMD_dh.schemas.defs.dataset_generic import n_frames

fnum = MetadataItemDescriptor(name="fnumber",
                              unit=None,
                              python_type=float,
                              comment="f-number setting of lens (as set, not calculated)")

flen = MetadataItemDescriptor(name="focal_length",
                              unit="mm",
                              python_type=float,
                              comment="focal length of the lens (as per manufacturer")

wdist = MetadataItemDescriptor(name="working_distance_setting",
                               unit="mm",
                               python_type=float,
                               comment="working distance setting of lens")


diffuser = MetadataItemDescriptor(name="diffuser",
                                  unit=None,
                                  python_type=bool,
                                  comment="was a diffuser used in the shear experiment")

defocus_pos = MetadataItemDescriptor(name="defocus_stage_pos",
                                     unit="um",
                                     python_type=float,
                                     comment="position of the defocus stage")



shear_angle = MetadataItemDescriptor(name="shear_angle",
                                     unit="degrees",
                                     python_type=float,
                                     comment="rotation of the lens under test")


shear_image = ArtifactType(name="shear_image",
                           afact_metadata_desc=frozenset([
                               exposure_time_s,
                               shear_angle,
                               n_frames
                               ]))


shear_dset = DatasetType(name="shear_test_data",
                         dset_class=DatasetClass.EXPERIMENTAL,
                         storage_desc = None,
                         dset_metadata_desc = None
                         )
