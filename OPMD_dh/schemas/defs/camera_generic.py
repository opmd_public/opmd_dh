from OPMD_dh.schemas.metadata import MetadataItemDescriptor

exposure_time_s = MetadataItemDescriptor(name="exposure_time",
                                         unit="s",
                                         python_type=float,
                                         comment="exposure time in seconds")

wavelength_nm = MetadataItemDescriptor(name="wavelength",
                                       unit="nm",
                                       python_type=float,
                                       comment="wavelength of illumination")

cam_temp = MetadataItemDescriptor(name="cam_temp",
                                  unit="C",
                                  python_type=float,
                                  comment="reported temperature of sensor/camera")


