from OPMD_dh.schemas.metadata import MetadataItemDescriptor

n_frames = MetadataItemDescriptor(name="nframes",
                                  unit=None,
                                  python_type=int,
                                  comment="number of illuminated frames in a flat field type dataset.")

n_dframes = MetadataItemDescriptor(name="ndframes",
                                   unit=None,
                                   python_type=int,
                                   comment="number of dark frames in the dataset")

n_biases = MetadataItemDescriptor(name="nbiases",
                                  unit=None,
                                  python_type=int,
                                  comment="number of bias frames in a dataset")

