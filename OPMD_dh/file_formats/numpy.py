from .base import OPMDArrayRep, OPMDTableRep, OPMDDataTypes, OPMDDataFile
import numpy as np
from typing import Union, Optional, Mapping, Any
from functools import cached_property
from types import MappingProxyType
from warnings import warn
import pathlib
from ruamel.yaml import YAML, CommentedSeq

#todo: npz files

class OPMDNumpyData(OPMDArrayRep, OPMDTableRep, OPMDDataFile):
    FILE_SUFFIX = ".npy"
    def __init__(self, data: Union[np.ndarray, np.recarray], name: str,
                 metadata: Optional[Mapping[str, Any]]=None):
        super().__init__(name)
        self._metadata = metadata if metadata is not None else {}
        self._data = data

        if isinstance(data, np.ndarray):
            self._held_datatype = OPMDDataTypes.ARRAY
        elif isinstance(data, np.recarray):
            self._held_datatype = OPMDDataTypes.TABLE
        else:
            raise TypeError("data supplied is not an array (np.ndarray) or table (np.recarray)")

        self._identifier = None


    @property
    def metadata(self):
        return MappingProxyType(self._metadata)

    @property
    def mutable_metadata(self):
        return self._metadata

    def add_metadata_item(self, key, value):
        if key in self._metadata:
            raise KeyError(f"metadata with key {key} already exists in this datarep")
        self._metadata[key] = value

    def extend_metadata(self, items):
        if any( _ in self._metadata for _ in items.keys()):
            raise KeyError(f"metadata with key {key} already exists in this datarep")
        self._metadata |= items

    @property
    def held_datatype(self):
        return self._held_datatype

    @property
    def array(self):
        if self.held_datatype is not OPMDDataTypes.ARRAY:
            raise TypeError("this rep doesn't hold an array!")
        return self._data

    @property
    def table(self):
        if self.held_datatype is not OPMDDataTypes.TABLE:
            raise TypeError("this rep doesn't hold a table!")

    @classmethod
    def check(cls, *args, **kwargs):
        #numpy can't check a file without opening it
        return None

    @classmethod
    def _get_mdatpath(cls, identifier):
        if not isinstance(identifier, pathlib.Path):
            identifier = pathlib.Path(identifier)

        mdatfilename = f"{identifier.stem}_meta.json"
        mdatpath = identifier.parent / mdatfilename
        return mdatpath

    @classmethod
    def _load_metadata(cls, identifier):
        path = cls._get_mdatpath(identifier)
        try:
            with open(mdatpath, "r") as f:
                yaml = YAML(typ="rt")
                return yaml.load(f)
        except FileNotFoundError as err:
            return None


    @classmethod
    def load(cls, identifier, readonly, **kwargs):
        arrdat = np.load(identifier, **kwargs)
        if readonly:
            arrdat.flags.writeable = False

        if isinstance(identifier, pathlib.Path):
            nm = identifier.stem
        else:
            nm = pathlib.Path(identifier).stem

        mdatpath = cls._get_mdatpath(identifier)

        mdat = cls._load_metadata(identifier)
        if mdat is None:
            warn(f"metadata file for file {identifier} was not found. Metadata will be empty")

        instance = cls(arrdat, nm, mdat)
        instance._identifier = identifier
        return instance

    def save(self, identifier: Path):
        np.save(identifier, self._data)
        mdatpath = self._get_mdatpath(identifier)
        yamldat = CommentedSeq()
        yamldat.yaml_set_start_comment(f"metadata file for the OPMDNumpy file {identifier.stem}")
        yaml = YAML(typ="rt")

        with open(mdatpath, "w") as f:
            yaml.dump(yamldat, f)

        self._identifier = identifier

    @classmethod
    def extract_metadata(cls, identifier: Path):
        mdatpath = cls._get_mdatpath(identifier)

    def construct_db_artifact(self, ctxt):
        if self._identifier is None:
            raise RuntimeError("this data has not been saved to disk. Cannot construct db artifact")
        afact_base = self._db_artifact_helper(ctxt, self._identifier)
        return afact_base

