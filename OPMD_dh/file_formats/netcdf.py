#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  7 10:50:32 2022

@author: danw
"""

#TODO: find typestubs for netCDF4
import netCDF4 #type: ignore
from .base import Path, T
from typing import Type, ClassVar, Optional, Generator, Set, Dict, Any
from abc import ABCMeta, abstractmethod
import numpy as np
from importlib.metadata import version
from pathlib import Path




class OPMDImageDataRep)OPMDDataItemRep):
    @property
    @abstractmethod
    def data(self) -> np.ndarray: ...

    @property
    @abstractmethod
    def metadata(self) -> Mapping[str,Any] : ...


class OPMDFileFormat(metaclass=ABCMeta):
    @classmethod
    @abstractmethod
    def load(cls: Type[T], pathrep: Path, readonly: bool=True, **kwargs) -> T: ...

    @classmethod
    @abstractmethod
    def check(cls: Type[T], pathrep: Path) -> bool: ...

    @classmethod
    @abstractmethod
    def create(cls: Type[T], pathrep: Path, overwrite: bool=False, **kwargs) -> T: ...

    @abstractproperty
    def metadata(self: T) -> Dict[str,Any]: ...



class NCDictProxy:
    def __init__(self, ncdset):
        self._obj = ncdset

    def __getitem__(self, idx):
        return self._obj.__dict__[idx]

    def __setitem__(self, idx, val):
        setattr(self._obj, idx, val)

    def __contains__(self, idx):
        return idx in self._obj.ncattrs()

    def __repr__(self):
        return repr(self._obj.__dict__)

    def items(self):
        return self._obj.__dict__.items()

class NCImageDataRep(OPMDImageDataRep):
    def __init__(self, vr: netCDF4.Variable, nm: str=None):
        self._vr = vr
        self._nm = nm

    @property
    def name(self) -> str:
        return self._nm
    
    @property
    def data(self) -> np.ndarray:
        return np.array(self._vr)

    @property
    def metadata(self) -> NCDictProxy:
        return NCDictProxy(self._vr.__dict__)


class NetCDFMixin(metaclass=ABCMeta):
    GROUP_NAME: ClassVar[Optional[str]] = None
    GROUP_NAMES: ClassVar[Set[str]] = set(["metadata"])
    __slots__ = ("_dstore", "_groups")

    def __new__(cls, *args, **kwargs):
        cls.GROUP_NAMES.add(cls.GROUP_NAME)
        return super().__new__(cls)

    def __init__(self, dstore: netCDF4.Dataset, *args, **kwargs):
        self._dstore = dstore
        self._groups = []

    def __del__(self):
        self._dstore.close()
        
    @classmethod
    def create(cls, pathrep: Path, overwrite: bool=False, **kwargs):
        modestr: str = "w" if overwrite else "x"
        dstore = netCDF4.Dataset(pathrep, modestr)
        instance = cls(dstore, **kwargs)
        instance._ensure_groups(True)
        return instance

    @classmethod
    def load(cls, pathrep: Path, readonly: bool=True, **kwargs):
        modestr = "r" if readonly else ""
        dstore = netCDF4.Dataset(pathrep, modestr)
        instance = cls(dstore, **kwargs)
        instance._ensure_groups(False)
        return instance

    def _ensure_groups(self, create: bool) -> None:
        for nm in self.GROUP_NAMES:
            if nm is None:
                raise NotImplementedError("subclass must override GROUP_NAME")
            elif nm not in self._dstore.groups:
                if create:
                    grp = self._dstore.createGroup(nm)
                    self._groups.append(grp)
                else:
                    raise IndexError(f"required group {nm} not present in file")

    @classmethod
    def _get_group(cls, instance: T) -> netCDF4.Group:
        return instance._dstore[f"/{cls.GROUP_NAME}"]


class NetCDFImage(NetCDFMixin, OPMDImageFile):
    GROUP_NAME = "images"
    def __init__(self, dstore: netCDF4.Dataset, **kwargs):
        super().__init__(dstore)

    @property
    def _imcount(self) -> int:
        grp = self._get_group(self)
        return len(grp.variables)


    def add_image(self, data: np.ndarray, meta: Dict[str, Any]):
        grp = self._get_group(self)

        if len(data.shape) not in [2,3]:
            raise ValueError("only 2/3 dimensional images supported so far")
        elif len(data.shape) == 3:
            nms = ["channel","y","x"]
        elif len(data.shape) == 2:
            nms = ["y","x"]

        if len(grp.dimensions) == 0:
            dims = tuple([grp.createDimension(nm,ln).size for nm,ln in zip(data.shape, nms)])
        else:
            dims = tuple([grp.dimensions[_].size for _ in grp.dimensions])

        if dims != data.shape:
            raise ValueError("dimension size doesn't match size of provided image")

        vrname = f"image{self._imcount}"
        imdat = grp.createVariable(vrname, data.dtype, dimensions=list(grp.dimensions.keys()))

        imdat[:] = data[:]

        for k,v in meta.items():
            setattr(imdat, k, v)

        #TODO: new stuff
        return imdat

    @classmethod
    def check(cls, pathrep: Path) -> bool:
        return pathrep.suffix == ".H5"

    @property
    def filemeta(self):
        return NCDictProxy(self._dstore)

    @property
    def images(self):
        grp = self._get_group(self)
        ims = [NCImageDataRep(v, k) for k,v in grp.variables]
        return ims

class NetCDFTables(NetCDFMixin, OPMDTableFile):
    GROUP_NAME = "tables"
    def __init__(self, dstore: netCDF4.Dataset, **kwargs):
        super().__init__(dstore)
