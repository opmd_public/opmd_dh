from .base import ArrayRep, TableRep, Path, DataFile, DataItemRep, OPMDDataTypes
from h5py import File, Group
import logging
import random
import string
from typing import Union, Any, Dict, Optional
from types import MappingProxyType
from functools import wraps
from collections.abc import MutableMapping
from itertools import chain
import numpy as np
from warnings import warn
from fsspec.core import OpenFile

def _ensure_basegrp(func):
    @wraps(func)
    def f(self, *args, **kwargs):
        assert hasattr(self, "_basegrp")
        return func(self, *args, **kwargs)
    return f

class H5OPMDMixin:

    @classmethod
    def count_nonmeta_subgroups(cls, obj: Union[File, Group]) -> int:
        cnt: int = 0

        def visitor(name):
            leafname = name.rsplit("/", 1)
            if len(leafname)>1:
                leafname = leafname[1]
            else:
                leafname = leafname[0]
            
            if leafname not in ["_opmdmeta", "metadata", "arrays", "tables"]:
                nonlocal cnt
                cnt += 1

        obj.visit(visitor)
        return cnt
        
    @classmethod
    def leafname(cls, obj: Union[File, Group]) -> str:
        if "NAME" in obj.attrs:
            # name for FILE is stored in a special group
            return obj.attrs["NAME"]
        else:
            # name for subgroups is just the name itself
            return obj.name.rsplit("/", 1)[1]

    @classmethod
    def _check_init_meta(cls, obj: Union[File, Group], name: str) -> Optional[Group]:
        if "metadata" not in obj:
            mdatgrp = obj.create_group("metadata")
            if not isinstance(obj, File):
                # hard link our metadata group into the top level metadata group
                obj.file["/metadata"][name] = mdatgrp
            return mdatgrp

        return obj["metadata"]

    @classmethod
    def _check_init_opmd_meta(cls, obj: Union[File, Group], name: str, create: bool) -> Optional[Group]:        
        if "OPMD_H5_VERSION" not in obj.attrs:
            if not create:
                return None
            obj.attrs["OPMD_H5_VERSION"] = cls.OPMD_H5_VERSION
            obj.attrs["OPMD_H5_CLASSNAME"] = cls.__qualname__
            obj.attrs["OPMD_H5_CLASS_MODULE"] = cls.__module__
            if isinstance(obj, File):
                # only want array and table groups if it's a File
                arrgrp = obj.create_group("arrays")
                tblgrp = obj.create_group("tables")
                # want name and metadata in all objects
                obj.attrs["NAME"] = name
        return obj.attrs

    @classmethod
    def extract_hdf_metadata(cls, obj: Union[File, Group]) -> Dict[str, Any]:
        mdatdct = dict(obj.attrs)
        thisdct = [mdatdct]

        def visitor(name, obj):
            nonlocal thisdct
            # print(name)
            # print(obj)
            # print(obj.name)
            newdct = dict(obj.attrs)
            thisdct[0][name] = newdct
            thisdct[0] = newdct
            return None

        # need to visit top level manually, .visititems doesn't do it
        obj.visititems(visitor)
        return mdatdct

    @property
    @_ensure_basegrp
    def metadata(self):
        if "metadata" in self._basegrp:    
            return H5MetadataMapper(self._basegrp["metadata"], False)
        else:
            return H5MetadataMapper(self._basegrp, False)
#        return MappingProxyType(self.extract_hdf_metadata(self._fl["/metadata"]))

    @property
    @_ensure_basegrp
    def mutable_metadata(self):
        if hasattr(self._basegrp, "keys"):
            if "metadata" in self._basegrp.keys():
                return H5MetadataMapper(self._basegrp["metadata"], True)
            else:
                raise ValueError("group does not contain a metadata group")
        else:
            return H5MetadataMapper(self._basegrp, True)

    @property
    @_ensure_basegrp
    def name(self):
        return self.leafname(self._basegrp)

    @name.setter
    @_ensure_basegrp
    def name(self, val: str):
        if isinstance(self._basegrp, File):    
            self._basegrp.attrs["NAME"] = val
        else:
            raise NotImplementedError("can't set NAME on a non-file object!")

    def add_metadata_item(self, key: str, value: Any):
        if key in self._basegrp["metadata"].attrs:
            raise KeyError("refusing to overwrite existing key")
        self._basegrp["metadata"].attrs[key] = value

    def extend_metadata(self, items):
        if any(_ in self._basegrp["metadata"].attrs for _ in items.keys()):
            raise KeyError("refusing to overwrite existing metadata key")
        self._basegrp["metadata"].attrs |= items


class H5OPMDFile(H5OPMDMixin, DataFile):
    OPMD_H5_VERSION = 1
    FILE_SUFFIX = ".H5"

    def __init__(self, name: str, fl: File = None, create_metas: bool = False):
        super().__init__(name)
        self._logger = logging.getLogger(type(self).__name__)
        if fl is None:
            flnam = "".join(random.choices(string.ascii_lowercase, k=100))
            self._logger.info("creating HDF5 file only in memory")
            self._fl = File(flnam, "w", driver="core", backing_store=False)
            self._memonly: bool = True
            self._check_init_meta(self._fl, create_metas)
            self._check_init_opmd_meta(self._fl, name, True)
        else:
            self._fl = fl
            self._memonly: bool = False
            self._check_init_meta(self._fl, create_metas)
            self._check_init_opmd_meta(self._fl, name, create_metas)

        self._basegrp = self._fl["/"]

    @classmethod
    def _empty_h5_file_on_disk(cls, identifier: Path,
                               allow_overwrite: bool = False, **kwargs):
        ostr: str = "w" if allow_overwrite else "x"
        fl = File(identifier, ostr, **kwargs)
        return fl

    @classmethod
    def load(cls, identifier: Path, readonly: bool = False, **kwargs):
        ostr: str = "r" if readonly else "r+"

            
        
        fl = File(identifier, ostr, **kwargs)
        
        if not "NAME" in fl.attrs:
            warnings.warn("no NAME in top level group of File, might not be an OPMD H5 file!")
            name = "FIXMEFIXME"
        else:
            name = fl.attrs["NAME"]

        instance = cls(name=name, fl=fl)
        if ifinstance(identifier, OpenFile):
            instance._fobj = identifier
        
        return instance

    @classmethod
    def check(cls, identifier: Path) -> Optional[bool]:
        fl = File(identifier, "r")
        
        grp = cls._check_init_opmd_meta(fl, None, False)
        return grp is not None

    @classmethod
    def create_on_disk(cls, identifier: Path, allow_overwrite: bool = False, **kwargs):
        fl = cls._empty_h5_file_on_disk(identifier, allow_overwrite)
        return cls(name="FIXMEFIXME", fl=fl, create_metas=True)

    @classmethod
    def extract_metadata(cls, identifier: Path, *args, **kwargs):
        fl = File(identifier, "r")
        return dict(cls.extract_hdf_metadata(fl))

    def save(self, identifier: Path=None, allow_overwrite: bool=False, **kwargs) -> None:
        if self._memonly:
            destfl = self._empty_h5_file_on_disk(identifier, allow_overwrite)
            for obj in self._fl.keys():
                self._fl.copy(obj, destfl)
            for k,v in self._fl.attrs.items():
                destfl.attrs[k] = v
            destfl.flush()
        else:
            self._fl.flush()

    def _get_group_for_datatype(self, tp: OPMDDataTypes) -> Group:
        if tp == OPMDDataTypes.ARRAY:
            return self._fl["arrays"]
        elif tp == OPMDDataTypes.TABLE:
            return self._fl["tables"]
        else:
            raise NotImplementedError("don't know about this data type")

    def _get_cls_for_datatype(self, tp: OPMDDataTypes):
        if tp == OPMDDataTypes.ARRAY:
            return H5Array
        elif tp == OPMDDataTypes.TABLE:
            return H5Table
        else:
            raise NotImplementedError("don't know about this data type")

    def construct_db_artifact(self, ctxt):
        raise NotImplementedError("it's late at night")

    def has_children(self, childtype = None):
        if childtype is not None:
            grp = self._get_group_for_datatype(childtype)
            return self._count_nonmeta_subgroups(grp) > 0

        tot = 0
        for grp in [self._fl["arrays"], self._fl["tables"]]:
            tot += self._count_nonmeta_subgroups(grp)
        return tot > 0

    def __del__(self):
        self._fl.close()
        if hasattr(self, "_fobj"):
            self._fobj.close()

    def __len__(self):
        return len(self._obj.attrs) + len(self._obj)
    
    @property
    def held_datatypes(self):
        out = []
        if self.count_nonmeta_subgroups(self._fl["arrays"]) > 0:
            out.append(OPMDDataTypes.ARRAY)
        if self.count_nonmeta_subgroups(self._fl["tables"]) > 0:
            out.append(OPMDDataTypes.TABLE)
        return out
    
    def _data_one_helper(self, dtype):
        grp = self._get_group_for_datatype(dtype)
        cls_ = self._get_cls_for_datatype(dtype)
                
        non_meta_grps = [_ for _ in grp if _ != "metadata"]
        if len(non_meta_grps) > 1:
            raise ValueError("item holds multiple data items, can't use get_data method on it")
        elif len(non_meta_grps) == 0:
            raise ValueError("item doesn't hold data of this type")
        else:
            return cls_, grp[non_meta_grps[0]], grp
    
    def get_data(self, dtype: Optional[OPMDDataTypes] = None):
        if len(self.held_datatypes) > 1:
            raise ValueError("multiple types of data held, can't use this method to retrieve them!")
        elif len(self.held_datatypes) == 1:
            dtype = self.held_datatypes[0]
        elif len(self.held_datatypes) ==0 :
            raise ValueError("this object doesn't hold any data!")
            
        cls_, grp, basegrp = self._data_one_helper(dtype)
        print("get_data")
        print(type(grp))
        return cls_(grp)


    def add_data(self, name: str, data, dtype=None):
        if dtype is None:
            dtype = OPMDDataTypes.from_data_instance(data)
        
        class_ = self._get_cls_for_datatype(dtype)
        grp = self._get_group_for_datatype(dtype)
        
        if name in grp:
            raise KeyError(f"data with name {name} already exists in group {grp}")
        
        return class_.create_in_file(self._fl, name, data)
        
        
    def set_data(self, data, dtype):
        if dtype is None:
            dtype = OPMDDataTypes.from_data_instance(data)
        
        cls_, grp, basegrp = self._data_one_helper(dtype, False)        
        cls_(grp, basegrp).data = data

    def iter_data(self):
        def iterfun(basegrp, tp):
            for name, item in basegrp.items():
                if name != "metadata":
                    yield tp(item)
        
        yield from iterfun(self._fl["arrays"], H5Array)
        yield from iterfun(self._fl["tables"], H5Table)
        
class H5Array(H5OPMDMixin, ArrayRep):
    def __init__(self, obj: Group):
        print("in init")
        print(type(obj))
        self._obj = obj
        self._basegrp = obj

    @classmethod
    def create_in_file(cls, fl: File, name: str, data: np.ndarray):
        dset = fl["arrays"].create_dataset(name, data=data)
        print(type(dset))
        return cls(obj=dset)

    def add_data(self, name:str, data, dtype=None):
        raise NotImplementedError("can't add_data to a H5Array")
    
    @property
    def array(self):
        return np.array(self._obj)
    
    def get_data(self, dtype=None):
        if dtype is None:
            dtype = OPMDDataTypes.ARRAY
        elif dtype is OPMDDataTypes.TABLE:
            raise ValueError("can only support array data")
            
        return self.array
        
    def set_data(self, dtype=None):
        raise NotImplementedError("setting data in a H5Array isn't supported, use add_data on the file object instead")
        
    def iter_data(self):
        raise NotImplementedError("can't iterate data on H5Array, it contains only a single array")

    def __repr__(self):
        return f"H5Array[ {self.name}]"

class H5Table(H5OPMDMixin, TableRep):
    def __init__(self, obj: Group):
        self._obj = obj
        self._basegrp = obj
        
    @classmethod 
    def create_in_file(cls, fl: File, name: str, data: np.recarray):
        tblgrp = fl["tables"].create_group(name)
        for colname in data.dtype.names:
            tblgrp.create_dataset(colname, data=data[colname])
        return cls(obj=tblgrp)

    def add_data(self, name: str, dtype=None):
        raise NotImplementedError("can't add_data to a H5Table")
    
    def get_data(self, dtype=None):
        if dtype is None:
            dtype = OPMDDataTypes.TABLE
        elif dtype is OPMDDataTypes.ARRAY:
            raise ValueError("can only support table data")
        
        arrs = [np.array(_) for _ in self._obj]
        names = [_.name for _ in self._obj]
        return np.rec.fromarrays(arrs, names=names)
    
    def set_data(self, dtype=None):
        raise NotImplementedError("setting data in a H5Table isn't supported, use add_data on the file object instead")
        
    def iter_data(self):
        raise NotImplementedError("can't iterate data on a H5Table, it contais only a single table")
        
    def __repr__(self):
        return f"H5Table[{self.name}]"
    
    @property
    def table(self):
        return self.get_data()


class H5MetadataMapper(H5OPMDMixin, MutableMapping):
    def __init__(self, obj: Group, writeable: bool=True):
        self._obj = obj
        self._writeable = writeable

    def __getitem__(self, key: str):
        if key in self._obj.attrs:
            return self._obj.attrs[key]
        elif key in self._obj:
            return type(self)(self._obj[key], self._writeable)

    def __setitem__(self, key: str, val: Any):
        if not self._writeable:
            raise AttributeError("Not writeable metadata view")
        if hasattr(self._obj, "keys"):
            if key in self._obj.keys():
                raise KeyError("can't overwrite whole groups of metadata")
        self._obj.attrs[key] = val

    def __delitem__(self, key: str):
        if not self._writeable:
            raise AttributeError("not writeable metadata view")
        if key in self._obj.attrs:
            del self._obj.attrs[key]
        elif key in self._obj:
            raise KeyError("can't delete whole group of metadata")
        else:
            raise KeyError(key)

    def __repr__(self):
        return f"MetadataView[{self._obj}]"

    def __len__(self):
        return len(self._obj.attrs) + len(self._obj)

    def __iter__(self):
        return chain(self._obj.attrs, self._obj)
