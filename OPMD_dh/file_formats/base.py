#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  7 10:49:50 2022

@author: danw
"""

from abc import  abstractmethod, ABCMeta
from typing import Union, Type, Dict, Any, TypeVar, Type, List, Mapping, Optional, Container, Iterator
import pathlib
import numpy as np
from enum import Enum
from ..metadata.dbmodel import Artifact
from ..datahandler import DHContext
from fsspec.core import OpenFile
import hashlib


Path = Union[str, pathlib.Path, OpenFile]
T = TypeVar("T", bound="OPMDFileFormat")


_ARRAY_DATA_TYPES = [np.ndarray]
_TABLE_DATA_TYPES = [np.recarray]

class OPMDDataTypes(Enum):
    ARRAY = 1
    TABLE = 2
    
    @classmethod
    def from_data_instance(cls, data):
        return cls.from_data_type(type(data))
    
    @classmethod
    def from_data_type(cls, dtp):
        if dtp in _ARRAY_DATA_TYPES:
            return OPMDDataTypes.ARRAY
        elif dtp in _TABLE_DATA_TYPES:
            return  OPMDDataTypes.TABLE
        elif any(issubclass(dtp,_) for _ in _ARRAY_DATA_TYPES):
            return OPMDDataTypes.ARRAY
        elif any(issubclass(dtp,_) for _ in _TABLE_DATA_TYPES):
            return OPMDDataTypes.TABLE
        elif issubclass(dtp, ArrayRep):
            return OPMDDataTypes.ARRAY
        elif issubclass(dtp, TableRep):
            return OPMDDataTypes.TABLE            
        else:
            raise ValueError(f"can't derive OPMDDataType from data with type {dtp}")


class DataItemRep(metaclass=ABCMeta):
    def __init__(self, name: str, *args, **kwargs):
        self._name = name

    @property
    @abstractmethod
    def metadata(self) -> Mapping[str, Any] : ...

    @property
    def name(self) -> str:
        return self._name

    @property
    def data(self):
        return self.get_data()

    @data.setter
    def data(self, data): 
        self.set_data(data)

    @abstractmethod
    def get_data(self, dtype: Optional[OPMDDataTypes] = None): ...

    @abstractmethod
    def set_data(self, data,  dtype: Optional[OPMDDataTypes] = None): ...

    @abstractmethod
    def add_metadata_item(self, key: str, value: Any): ...

    @abstractmethod
    def extend_metadata(self, items: Mapping[str, Any]): ...

    @abstractmethod
    def iter_data(self, dtype: Optional[OPMDDataTypes] = None): ...

    @property
    def held_datatypes(self) -> List[OPMDDataTypes]:
        if hasattr(self, "DATA_TYPE"):
            return [self.DATA_TYPE]
        return []

    @abstractmethod
    def add_data(self, name: str, data, dtype: Optional[OPMDDataTypes]=None):
        raise NotImplementedError("this class doesn't support multiple data items")

    @property
    def mutable_metadata(self):
        raise NotImplementedError("this class doesn't allow you to mutate its metadata")

    @property
    def has_children(self, childtype: Optional[OPMDDataTypes]) -> bool:
        return False

    def iter_children(self, datatypes: Optional[Container[OPMDDataTypes]] = None) -> Iterator:
        raise NotImplementedError("this class can't iterate over children")

class DataFile(DataItemRep):

    @classmethod
    @abstractmethod
    def load(cls, identifier: Path, readonly: bool=False,  **kwargs) -> DataItemRep: ...

    @classmethod
    @abstractmethod
    def check(cls, identifier: Path) -> Optional[bool]: ...

    @classmethod
    def identify(cls, identifier: Path) -> bool:
        if not isinstance(identifier, pahlib.Path):
            identifier = pathlib.Path(identifier)
        assert hasattr(cls, "FILE_SUFFIX")
        
        if identifier.suffix == cls.FILE_SUFFIX:
            return True
        return False

    @classmethod
    @abstractmethod
    def create_on_disk(cls, identifier: Path, allow_overwrite: bool=False, **kwargs) -> DataItemRep:
        raise NotImplementedError("this file type can't create empty files")

    @classmethod
    @abstractmethod
    def extract_metadata(cls, identifier: Path, *args, **kwargs) -> Mapping[str, Any]: ...

    @abstractmethod
    def save(self, identifier: Path, **kwargs) -> None: ...

    @abstractmethod
    def construct_db_artifact(self, ctxt: DHContext) -> Artifact: ...

    def _db_artifact_helper(self, ctxt: DHContext, identifier: Path) -> Artifact:
        if not isinstance(identifier, pathlib.Path):
            identifier = pathlib.Path(identifier)

        if not identifier.is_absolute():
            identifier = identifier.resolve(strict=True)

        if not identifier.is_relative_to(ctxt.basepath):
            raise ValueError("artifact is not relative to repo base path")

        afact_base = Artifact(path=identifier.relative_to(ctxt.basepath),
                              afact_metadata=dict(self.metadata))

        return afact_base


    
class ArrayRep(DataItemRep):
    DATA_TYPE: OPMDDataTypes = OPMDDataTypes.ARRAY
    
    @property
    @abstractmethod
    def array(self) -> np.ndarray: ...
    
    @array.setter
    @abstractmethod
    def array(self, arr) -> None: ...
    
class TableRep(DataItemRep):
    DATA_TYPE: OPMDDataTypes = OPMDDataTypes.TABLE

    @property
    @abstractmethod
    def table(self) -> np.recarray: ...


    @table.setter
    @abstractmethod
    def table(self, tbl) -> None: ...


