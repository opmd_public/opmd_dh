import pathlib
from .helpers import find_datalad_repo_root, YamlFileProxy
from ruamel.yaml import YAML, CommentedSeq, add_representer, RoundTripDumper
from typing import Optional, Union, Type, TypeVar, List, Iterable
from click import prompt, confirm
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from sqlalchemy.orm import Session, Query, sessionmaker, joinedload, scoped_session
from .helpers import _resolvepath
from sqlalchemy import select, inspect, create_engine
from .metadata.dbmodel import BaseModel, Operator
import warnings
from collections import OrderedDict
import importlib
from collections.abc import Iterable
from .operator_utils import ConfigManager
import json


T = TypeVar("T", bound=SQLAlchemyAutoSchema)
R = TypeVar("R", bound=BaseModel)




class DHContext:
    def __init__(self, datalad_repo, sqlalchemy_engine, sessiontp=None):
        self._repo = datalad_repo
        self._dbengine = sqlalchemy_engine

        if sessiontp is None:
            sessiontp = sessionmaker(bind=sqlalchemy_engine)

        self._sessiontp = sessiontp

    @property
    def basepath(self) -> pathlib.Path:
        return pathlib.Path(self._repo.path).resolve(strict=True)

class DHTool:
    @classmethod
    def get_default_repo(cls, **kwargs):
        path = ConfigManager.get_default_repo()
        if path is None:
            raise ValueError("no default repo path is set! Try running dhtool setdefaultrepo $PATH_OF_REPO")
        return cls.from_path(path, **kwargs)


    @classmethod
    def from_path(cls, pth: Optional[Union[pathlib.Path, str]], **kwargs):
        repo = find_datalad_repo_root(pth)
        return cls(repo, **kwargs)

    @classmethod
    def from_dbfile(cls, repopath, dbfile, **kwargs):

        if repopath is None:
            repopath = ConfigManager.get_default_repo()
            if repopath is None:
                raise ValueError("no default repo path is set! Try running dhtool setdefaultrepo $PATH_OF_REPO")

        repo = find_datalad_repo_root(repopath)
        engine = create_engine(f"sqlite:///{dbfile}")
        instance = cls(repo, engine, **kwargs)
        instance._schemas_loaded = True
        return instance

    def __init__(self, datalad_repo, sqlalchemy_engine=None, sessiontp=None):
        """NOTE: must have checked for valid repo_root first!! Best not to call this directly"""
        self._repo = datalad_repo
        self._ensure_cfg_dir()

        if sqlalchemy_engine is None:
            sqlalchemy_engine = create_engine("sqlite:///:memory:")
            
            BaseModel.metadata.create_all(sqlalchemy_engine)
        if sessiontp is None:
            sessiontp = sessionmaker(bind=sqlalchemy_engine)

        self._dbengine = sqlalchemy_engine
        self._sessiontp = sessiontp
        self._modulecache = {}
        self._schemas_loaded: bool = False

        self.Session = scoped_session(sessiontp)

    def _ensure_cfg_dir(self) -> None:
        dh_path = pathlib.Path(self._repo.path) / ".dh" 
        if not dh_path.exists():
            dh_path.mkdir()
        cfgpath = dh_path / "config.yml"
        with open(cfgpath, "w") as f:
            default_config = {"schemafilemapping" : {}}
            yaml = YAML(typ="rt")
            yaml.dump(default_config, f)

        self._repo.add(str(cfgpath), git=True)
        self._repo.commit(msg="[OPMD-dh]: add empty config file")

    @property
    def basepath(self) -> pathlib.Path:
        return pathlib.Path(self._repo.path)

    @property
    def meta_base_path(self) -> pathlib.Path:
        return pathlib.Path(self.config.get("standalone_metapath", "dh_meta"))

    @property
    def standalone_schemas_path(self) -> pathlib.Path:
        schemastr = self.config.get("standalone_metapath", "dh_meta")
        return self.basepath / schemastr

    @property
    def loaded_engine(self):
        if not self._schemas_loaded:
            self.load_all_schemas()
        return self.engine

    @property
    def engine(self):
        return self._dbengine

    def get_config(self, unsafe: bool=False):
        cfgpath = (pathlib.Path(self._repo.path) / ".dh") / "config.yml"
        with open(cfgpath, "r") as f:
            yaml = YAML(typ="unsafe") if unsafe else YAML(typ="rt")
            dat = yaml.load(f)
        return dat

    @property
    def config(self):
        return self.get_config()

    @property
    def unsafe_config(self):
        return self.get_config(unsafe=True)

    @property
    def shared_operators(self) -> bool:
        sharedraw = self._repo.config.get("opmd-dh.shared-operators")
        if sharedraw is None:
            warnings.warn("shared-operators not set in this repo, defaulting to shared for safety...")
            self._repo.config.set("opmd-dh.shared-operators", str(int(True)), scope="local")
            sharedraw = "1"
        shared = bool(int(sharedraw))
        return shared

    @property
    def linked_dbs(self) -> List[str]:
        linkdbraw = self._repo.config.get("opmd-dh.linked_dbs")
        return json.loads(linkdbraw)

    @linked_dbs.setter
    def linked_dbs(self, linkeddbs: List[str]) -> None:

        if not isinstance(linkeddbs, list):
            raise TypeError("must pass a list of strings to linkeddbs")

        linkeddbs = [str(_) for _ in linkeddbs]
        linkdbraw = json.dumps(linkeddbs)
        self._repo.config.set("opmd-dh.linked_dbs", linkdbraw)

    # def _ensuresession(self, session=None, **kwargs):
    #     if session is None and self._sessiontp is None:
    #         raise RuntimeError("must supply either a sessiontp in constructor or a session objects")
    #     elif session is None:
    #         return self._sessiontp(**kwargs)
    #     return session

    @property
    def config_edit(self):
        cfgpath = (self._repo.path / ".dh") /"config.yml"
        return YamlFileProxy(cfgpath, True)


    @shared_operators.setter
    def shared_operators(self, val: bool) -> None:
        self._repo.config.set("opmd-dh.shared-operators", value=str(int(val)), scope="local")


    def interactive_create(self, schematp: Type[T], session=None) -> T:
        data = {}
        flditer = sorted(schematp().fields.items(), key=lambda kv: kv[1].metadata.get("interactive_order",len(schematp().fields)+1))

        for nm, fld in flditer:
            if fld.metadata.get("interactive_create") in (True, None):
                if fld.required:
                    value = prompt(f"enter a value for the field {nm}")
                else:
                    value = prompt(f"enter a value for the field {nm}", default="")

                data[nm] = fld.deserialize(value)

        with self.Session() as sess:
            des = schematp().load(data,session=sess)
        return des

    def load_all_schemas(self, session=None):
        yaml_gen = self.basepath.rglob("*.yml")

        if session is None:
            session = self.Session()
        
        for path in yaml_gen:
            print(f"found path {path}")
            try:
                items = self.yaml_file_load(path, session)
                session.add_all(items)
            except ValueError:
                warnings.warn(f"path {path} is a YAML file but not a valid dh_metadata file!")

            session.commit()


    def yaml_file_load(self, path, session):
        yaml = YAML(typ="rt")
        with open(path, "r") as f:
            dat = yaml.load(f)
        metatagfind = next((_ for _ in dat if "dh_meta_tag" in _), None)
        if metatagfind is None:
            raise ValueError("no dh_meta_tag in loaded yaml data")

        tgt_module = metatagfind["dh_meta_tag"][0]
        tgt_type = metatagfind["dh_meta_tag"][1]

        if tgt_module not in self._modulecache:
            self._modulecache[tgt_module] = importlib.import_module(tgt_module)
        mod = self._modulecache[tgt_module]
        schematp = getattr(mod, tgt_type)

        dat.pop(dat.index(metatagfind))
        return schematp(many=True).load(dat, session=session)

    def yaml_file_dump(self, listdat, schematp, path):
        yamldat = CommentedSeq(listdat)
        yamldat.insert(0, {"dh_meta_tag" : (schematp.__module__, schematp.__qualname__)})
        yamldat.yaml_set_start_comment("This file was created by OPMD-dh. You should probably not edit it manually unless you know what you are doing!")
        yaml = YAML(typ="rt")
        yaml.Representer.add_representer(OrderedDict, yaml.Representer.represent_dict)

        directory = path.parents[0]
        directory.mkdir(parents=True, exist_ok=True)

        with open(path, "w") as f:
            yaml.dump(yamldat, f)


    def dump_all_schemas(self, session=None):
        mapped_classes = BaseModel.registry.mappers
        cfg = self.config
        for cls in mapped_classes:
            dbtype = cls.class_
            marshtype = getattr(dbtype, "__marshmallow__", None)
            if marshtype is None:
                warnings.warn(f"schema type {dbtype} has no serialization class defined. This is a problem with the OPMD_dh code. Data might be lost")
                continue
            with self.Session() as sess:
                primkey = inspect(dbtype).primary_key[0]
                stmt = select(dbtype).order_by(primkey)
                objects = sess.execute(stmt).scalars().unique().all()
                if len(objects) == 0:
                    continue

            #NOTE: for now only support the case with all in the same file, or each one in a different file
                if getattr(marshtype, "STANDALONE_SCHEMA_FILE", False):
                    path = self.basepath / marshtype.get_classdumpfilepath(self)
                    serialized = marshtype(many=True).dump(objects)
                    self.yaml_file_dump(serialized, marshtype, path)

                else:
                    paths = { obj : self.basepath / obj.get_dumpfilepath(self) for obj in objects}
                    collected_objs = {}
                    for obj, path in paths.items():
                        if path not in collected_objs:
                            collected_objs[path] = []
                        collected_objs[path].append(obj)

                    for path, objects in collected_objs.items():
                        serialized = marshtype(many=True).dump(objects)
                        self.yaml_file_dump(serialized, marshtype, path)
