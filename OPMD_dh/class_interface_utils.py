import warnings
import sqlalchemy as sa

def required_attributes(cls, attrnames):
    defaults = getattr(cls, "DEFAULT_ATTRIBUTES" , None)
    for attrname in attrnames:
        if not hasattr(cls, attrname):
            if defaults is None or attrname not in defaults:
                raise KeyError(f"required attribute {attrname} not found in class and no default set!")
            else:
                if isinstance(defaults[attrname], str):
                    stfmt = defaults[attrname].format(cls=cls)
                    warnings.warn(f"no {attrname} specified, using default of {stfmt}")
                    setattr(cls, attrname, stfmt)
                elif callable(defaults[attrname]):
                    res = defaults[attrname](cls)
                    warnings.warn(f"no {attrname} specified, using default of {res}")
                    setattr(cls, attrname, res)
                else:
                    raise ValueError(f"invalid default specified for attribute {attrname}")


def dbrep_generator(dbtype, query):
    def classfun(cls, dhtool, session=None, create_if_not_found: bool=True,
                 update_db_props: bool=True, **kwargs):
        eng = dhtool.loaded_engine
        sess = dhtool.Session()
        q = query(cls)
        stmt = sa.select(dbtype).where(q)
        obj = sess.execute(stmt).scalar_one_or_none()

        if obj is None:
            if not create_if_not_found:
                raise KeyError("no existing db entry found and not creating a new one!")
            else:
                obj = dbtype.from_python_class(cls)
                sess.add(obj)
        else:
            if update_db_props:
                newobj = dbtype.from_python_class(cls)
                sess.merge(newobj)
                sess.commit()

        return obj

    return classfun
