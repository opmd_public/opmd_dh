import pathlib
from typing import Optional, Union
from datalad.distribution.dataset import repo_from_path
from datalad.dataset.repo import RepoInterface
from contextlib import contextmanager
from ruamel.yaml import YAML

class NoDHRepoFoundError(Exception):
    pass

def _resolvepath(path: Optional[Union[pathlib.Path,str]]) -> pathlib.Path:
    if isinstance(path, str):
        path = pathlib.Path(path)
    elif path is None:
        path = pathlib.Path(".")

    return path.expanduser().resolve()

def find_datalad_repo_root(path: Optional[Union[pathlib.Path, str]] = None )-> RepoInterface:
    path = _resolvepath(path)

    def resolve_dl_repo(path):
        try:
            repo = repo_from_path(path)
            return repo
        except ValueError as err:
            return None

    while (repo := resolve_dl_repo(path)) is None:
        print(path)
        parent = path.parent.expanduser().resolve()
        if parent == path:
            raise NoDHRepoFoundError("no dh repo root found!")
        path = parent

    return repo

@contextmanager
def YamlFileProxy(path: Union[pathlib.Path, str], writeable: bool=False, defaultval={}, **kwargs):
    if not isinstance(path, pathlib.Path):
        path = pathlib.Path(path)

    yaml = YAML(**kwargs)

    if writeable and not path.exists():
        yamldata = defaultval
    else:
        with open(path, "r") as f:
            yamldata = yaml.load(f)

    print(f"yamldata before: {yamldata}")
    yield yamldata

    print(f"yamldata after: {yamldata}")

    if writeable:
        with open(path, "w") as f:
            yaml.dump(yamldata, f)
