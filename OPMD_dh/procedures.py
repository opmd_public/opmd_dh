import warnings
from datetime import datetime
from .metadata.dbmodel import ProcedureType, ProcedureClass, ExperimentStatus, Procedure, Operator
from .metadata.dbmodel import ProcedureStep, Artifact, ArtifactType, TravelerEntryTypes, TravelerEntry
from .metadata.schemas import ProcedureTypeSchema
import sqlalchemy as sa
import uuid
from .class_interface_utils import required_attributes, dbrep_generator
from .datahandler import DHTool
from abc import ABCMeta, abstractmethod
import logging
from typing import Iterable, Dict, Optional, Generic, TypeVar, Callable, get_origin, Any
from .operator_utils import OperatorManager
import inspect
from io import StringIO
import pathlib
from datetime import datetime
import pytz
import traceback
import sys

T = TypeVar("T")

class ParameterType(Generic[T]): ...

class DoNotStoreInDatabase(ParameterType[T]): ...

class DoNotPassToStep(ParameterType[T]): ...


class ProcedureBase(metaclass=ABCMeta):
    DB_PROCEDURE_MODEL_CLASS = Procedure
    DB_PROCEDURE_TYPE_CLASS = ProcedureType
    ARTIFACT_TYPE: str = None
    BASE_PATH_FORMAT: str = "{procedure.type.oneword_name}/{procedure.uuid}"
    PROCEDURE_CLASS = ProcedureClass.OTHER
    REQUIRED_PROCEDURE_TYPES = []
    REQUIRED_COMPONENT_TYPES = []
    DEFAULT_ATTRIBUTES = {"ONEWORD_NAME" : "{cls.__name__}",
                          "FRIENDLY_NAME" : "{cls.__name__}"}

    RECORD_TRAVELERS: bool = True

    def __init_subclass__(cls):
        required_attributes(cls, ["ONEWORD_NAME", "FRIENDLY_NAME"])


    def pack_argument_helper(self, remove_internals: bool=True):
        outerframe = inspect.currentframe().f_back
        args, vargs, kwargs, lcls = inspect.getargvalues(outerframe)
        outkwargs = {k : lcls[k] for k in args}
        if kwargs is not None:
            outkwargs.update(lcls[kwargs])

        if vargs is not None:
            raise TypeError("pack_argument_helper does not work with unnamed positional arguments")

        selfarg = next( k for k,v in outkwargs.items() if v is self)
        if selfarg is not None:
            outkwargs.pop(selfarg)

        if remove_internals:
            internals = {}
            func_outer = getattr(self, outerframe.f_code.co_name, None)
            if not isinstance(func_outer, Callable):
                raise TypeError("object is not a Callable! This is a pretty deep programming error")

            for paramname, annot in inspect.get_annotations(func_outer).items():
                origin = get_origin(annot)
                if origin is None:
                    continue

                if issubclass(origin, DoNotStoreInDatabase):
                    internals[paramname] = outkwargs.pop(paramname)    

            self._internals = internals

        return outkwargs


    def _lookup_artifact_type(self, sess, name: str) -> ArtifactType:
        qry = sa.select(ArtifactType).where(ArtifactType.name == name)
        res = sess.execute(qry).scalar_one_or_none()
        if res is None:
            raise ValueError(f" couldn't match artifact type with name {name}")

        return res

    def identify_artifact_type(self, afact, dbsess) -> ArtifactType:
        if self.ARTIFACT_TYPE is None:
            self.logger.error("no ARTIFACT_TYPE attribute provided, cannot identify artifact")
            raise ValueError("no ARTIFACT_TYPE attribute provided, cannot identify artifact")
        with self._dhtool.Session() as sess:
            qry = sa.select(ArtifactType)

    def generate_seqnums(self, iterable, **kwargs):
        yield from enumerate(iterable)

    @abstractmethod
    def setup_dut(self, existing_obj: Optional, **kwargs):
        """ should be overridden and should set up only the database aspects of the DUT.
        Setting actual hardware values should be deferred to the run_step and run_procedure functions"""
        pass

    def procedure_db_hook(self, proctp) -> None:
        """ Hook for modifying the Procedure database object (e.g. to add extra components or simliar) """
        pass


    @abstractmethod
    def setup_hw(self, dut, **parameter_values):
        pass

    def plan(self, **kwargs) -> Iterable[Dict]:
        """ plan out all the steps that will be run in this experiment.
            This should all be planned in advance and returned as a concrete type.
            This function can also be a generator function. 
            It should be possible to run extra steps in the run_step method if some steps will be dynamic,
            but in that case will not be possible to resume"""
        return [{}]

    def add_traveler_entry(self, notes: str, metadata: dict, type=TravelerEntryTypes.PROCEDURE_RUN,
                           component: Optional = None):

        if component is None:
            component = self._dut
        with self._dhtool.Session() as sess:
            travobj = TravelerEntry(
                notes=notes,
                traveler_metadata = metadata,
                procedure = self._procobj,
                entered_by = self.operator,
                entry_type=type,
                component=component)

            sess.add(travobj)
        return travobj


    def extract_artifact_metadata(self, afact, **kwargs) -> Dict[str, Any]:
        return {}

    @abstractmethod
    def finalize_artifact(self, path, afactobj):
        pass

    @abstractmethod
    def run_step(self, **kwargs):
        pass


    def cleanup(self, **kwargs):
        pass

    def preflight_check(self, **kwargs):
        self.logger.warn("pre-flight check method not overwridden, no sensible pre-flight checks will be done")

    def _plan_steps(self):
        self.logger.info("planning experimental steps...")
        params = self._procobj.parameter_values.copy()
        self.logger.debug(f"parameters: {params}")
        with self._dhtool.Session() as sess:
            for step in self.plan(**params):
                self.logger.debug(f"planning step with parameter values {step.items()}")
                stepobj = ProcedureStep(plan_data=step, procedure=self._procobj,
                                        status=ExperimentStatus.TODO)
                sess.add(stepobj)
            sess.commit()


   def postrun(self, exc: Optional, stepobj,  **kwargs):
        self.logger.debug("no postrun method override defined, not doing anything...")


    def __del__(self):
        self.cleanup()
        pass

    def run(self, stop_on_failed_step: bool=True, **extra_kwargs):
        internals = getattr(self, "_internals", {})
        allkws = internals |  self._procobj.parameter_values.copy()  | extra_kwargs


        self.logger.info("preparing hardware for experimental run...")
        self.setup_hw(self._dut, **allkws)

        with self._dhtool.Session() as sess:
            #find if a procedure step is already running
            runquery = sa.intersect(sa.select(ProcedureStep).where(ProcedureStep.procedure == self._procobj),
                                    sa.select(ProcedureStep).where(ProcedureStep.status == ExperimentStatus.RUNNING))
            running_step = sess.execute(runquery).scalar_one_or_none()
            if running_step is not None:
                raise RuntimeError(f"database indicates step with id {running_step.id} is already running! Bailing out")

            failquery = sa.intersect(sa.select(ProcedureStep).where(ProcedureStep.procedure == self._procobj),
                                     sa.select(ProcedureStep).where(ProcedureStep.status == ExperimentStatus.ERROR))
            fail_step = sess.execute(failquery).scalar_one_or_none()
            if fail_step is not None:
                raise RuntimeError(f"database indicates step with id {fail_step.id} failed in previous run! Resume not implemented yet...")

            qry = sa.and_(ProcedureStep.status == ExperimentStatus.TODO,
                    ProcedureStep.procedure == self._procobj)

            todo_query= sa.select(ProcedureStep).where(qry).order_by(ProcedureStep.procstepseq)
            todo_steps = list(sess.execute(todo_query).scalars())


            self.logger.info(f"# of steps still marked TODO: {len(todo_steps)}")
            self.logger.info("running preflight checks before starting experimental steps...")

            self.preflight_check(**allkws)

            afact_base_path = self._dhtool.basepath / pathlib.Path(self._procobj.artifact_path)


            self.logger.debug(f"session object in run(): {sess}")
            for stepobj in todo_steps:
                try:
                    self.logger.info(f"running step {stepobj.id} with parameters {stepobj.plan_data}")
                    stepobj.status = ExperimentStatus.RUNNING
                    stepobj.starttime = datetime.utcnow().replace(tzinfo=pytz.utc)

                    self.logger.debug(f"allkws: {allkws}")

                    dup_kws = ( _ for _ in allkws if _ in stepobj.plan_data)
                    for dupkw in dup_kws:
                        self.logger.warning(f" the keyword: {dupkw} appears both in the step plan data and the global experiment parameters. Step plan data overwrites global parameters. Consider renaming one to avoid future problems")

                    stepcallkws = allkws | stepobj.plan_data
                    afactgen = self.run_step(**stepcallkws)

                    for seqnum, artifact in self.generate_seqnums(afactgen, **stepcallkws):
                        self.logger.info("processing artifact...")
                        mdat = self.extract_artifact_metadata(artifact, **stepcallkws)
                        aftype = self.identify_artifact_type(artifact, sess)
                        if aftype is None:
                            raise ValueError("failed to identify artifact type, cannot continue!")

                        afpath_suggest = self._procobj.suggest_new_artifact_filename(
                            aftype, seqnum, stepobj, mdat)

                        afpath_full = afact_base_path / afpath_suggest
                        self.logger.debug(f"suggested file path for artifact is {afpath_full}")
                        self.logger.debug(f"is step object in session? {stepobj in sess}")
                        afobj = Artifact(path=afpath_suggest,
                                         afact_type = aftype,
                                         afact_metadata = mdat)
                        stepobj.artifacts.append(afobj)

                        self.logger.debug(f"calling finalize  to save artifact to disk")
                        self.finalize_artifact(afpath_full, artifact)

                except Exception as  err:
                    self.logger.error(f"running step {stepobj.id} resulted in an error!")
                    self.logger.error(f"the error was '{type(err)} - {err}'")
                    self.logger.error(f'traceback: "{traceback.format_tb(err.__traceback__)}"')
                    stepobj.status = ExperimentStatus.ERROR
                    exc_name, exc_value, _ = sys.exc_info()
                    if stop_on_failed_step:
                        raise err
                else:
                    stepobj.status = ExperimentStatus.COMPLETE
                    exc_value = None
                finally:
                    stepobj.endtime = datetime.utcnow().replace(tzinfo=pytz.utc)
                    sess.commit()
                    self.postrun(exc_value, stepobj, **allkws)


    @property
    def operator(self) -> Operator:
        with self._dhtool.Session() as sess:
            opid = OperatorManager.get_operator_id(self._dhtool)
            stmt = sa.select(Operator).where(Operator.id == opid)
            op = sess.execute(stmt).scalar_one()
        if op is None:
                raise ValueError("couldn't load operator for dhrepo, maybe it isn't set up properly")
        return op


    @classmethod
    def _get_proctypeobj(cls, dbsess, notes: str):
        proctypetyp= cls.DB_PROCEDURE_TYPE_CLASS
        clsname = cls.__qualname__
        modulename = __name__

        ptobj = proctypetyp.load_or_create(dbsess, lookup_kw="oneword_name",
                                           oneword_name = cls.ONEWORD_NAME,
                                           friendly_name = cls.FRIENDLY_NAME,
                                           base_file_path = cls.BASE_PATH_FORMAT,
                                           notes = notes,
                                           class_name = clsname,
                                           module_name = modulename,
                                           overwrite=True)

        return ptobj
                                           
    
    def __init__(self, dhtool, loadid: Optional[int] = None, notes: Optional[str]=None,
                 loglevel: int = logging.INFO, **parameters):
        self.logger = logging.getLogger(type(self).__name__)
        logfmt = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
        self.logger.setLevel(loglevel)


        tempbuf = StringIO()
        tmphandler = logging.StreamHandler(tempbuf)
        tmphandler.setFormatter(logfmt)
        tmphandler.setLevel(loglevel)
        self.logger.addHandler(tmphandler)


        self.logger.info(f"initializing run of: {self.FRIENDLY_NAME}")
        self._dhtool = dhtool
        self.logger.info(f"connected to DH repo at: {self._dhtool.basepath}")
        self.logger.info(f"DH repo database connection: {self._dhtool.engine.url}")

        self._parameters = parameters

        self.logger.debug("creating or loading ProcedureType relevant object...")
        with dhtool.Session() as sess:
            sess.expire_on_commit = False

            self._proctypeobj  = self._get_proctypeobj(sess, notes)
            if loadid is None:
                self.logger.info("No loadid specified, creating new procedure object in DB")
                dut = self.setup_dut(**parameters, existing_obj=None)
                self._dut = dut
                typ = self.DB_PROCEDURE_MODEL_CLASS
                procobj = typ(type=self._proctypeobj, device_under_test=dut,
                              dhtool=self._dhtool,
                              parameter_values=parameters)
                print("printing procobj _parameter_values")
                print(procobj._parameter_values)
                sess.add(procobj)
                sess.commit()
            else:
                self.logger.info("loadid specified, loading existing procedure object from DB")
                procobj = typ.load_or_create(sess, lookup_kw="id", id=loadid,
                                             dhtool=self._dhtool)
                if procobj is None:
                    raise KeyError("existing object to resume not found!")
                self._dut = self.setup_dut(procobj.device_under_test)

            self._procobj = procobj
            relpath = pathlib.Path(self._procobj.artifact_path)
            basepth = self._dhtool.basepath / relpath

            self.logger.debug(f"basepath: {basepth}")
            basepth.mkdir(parents=True, exist_ok=True)
            self.logger.info(f"UUID of procedure object is: {self._procobj.uuid}")
            self.logger.info(f"Procedure base storage location is {basepth}")
            logpth = basepth / "procedure_log.txt"

            self.logger.removeHandler(tmphandler)
            with open(logpth, "a") as f:
                tempbuf.seek(0)
                f.write(tempbuf.read())

            fhandler = logging.FileHandler(logpth)
            fhandler.setFormatter(logfmt)
            fhandler.setLevel(loglevel)
            self.logger.addHandler(fhandler)
            self._reloaded: bool =  loadid is not None

            if loadid is None:
                self._plan_steps()

            self.logger.info("initialization of procedure DB objects complete")

class ExperimentalProcedure(ProcedureBase):
    PROCEDURE_CLASS = ProcedureClass.EXPERIMENT

class AnalysisProcedure(ProcedureBase):
    PROCEDURE_CLASS = ProcedureClass.DERIVED

    def setup_hw(self, dut, parameter_values):
        pass
