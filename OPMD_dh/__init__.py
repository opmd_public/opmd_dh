from importlib.metadata import version

try:
    __version__ = version(__package__)
except:
    __version__ = "unknown"

