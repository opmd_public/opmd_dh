#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  8 13:51:56 2023

@author: weatherill
"""

from typing import Optional, Type, get_type_hints
from pint import Unit
from functools import wraps
from inspect import signature

def tag_metadata(fun=None, name: Optional[str]=None, typ: Optional[Type]=None, 
                 unit: Optional[Unit] = None, metakwargs: Optional[dict] = None):        

    if metakwargs is None:
        metakwargs = {}

    if fun is not None:
        assert callable(fun)

    def decorator(fun):

        usefun = fun.fget if isinstance(fun, property) else fun
        #determine name of metadata tag
        nonlocal name
        if name is None:
            name = usefun.__name__

        nonlocal typ
        if typ is None:
            #try to get return type, don't worry if it's None, can determine dynamically
            typ = get_type_hints(usefun).get("return")

        setattr(usefun, "_meta_name", name)
        setattr(usefun, "_meta_type", typ)
        setattr(usefun, "_meta_unit", unit)

        sig = signature(usefun)
        #assume there's a first "self" parameter

        nonlocal metakwargs
        if len(metakwargs) != len(sig.parameters) -1:
            raise ValueError("for functions that take arguments you must supply them all in the metadata tag")

        if len(metakwargs) > 0:
            setattr(usefun, "_meta_kwargs", metakwargs)

        return fun
    
    if fun:
        return decorator(fun)
    else:
        return decorator

def _get_meta_name(fun):
    if isinstance(fun, property):
        fun = fun.fget
    return getattr(fun, "_meta_name", None)

def _retrieve_metadata(fun, obj):
    if isinstance(fun, property):
        return fun.fget(obj)
    else:
        kwargs = getattr(fun, "_meta_kwargs", {})
        return fun(obj, **kwargs)


def has_metadata(cls):
    def collect_metadata(self):
        out = {}
        
        for meth in vars(type(self)).values():
            if (k := _get_meta_name(meth)) is not None:
                mitem = _retrieve_metadata(meth, self)
                out[k] = mitem

        return out
    setattr(cls, "collect_metadata", collect_metadata)    
    return cls


if __name__ == "__main__":

    @has_metadata
    class A:        
        @tag_metadata(metakwargs={"i" : 2})
        def meta_val(self, i) -> int:
            return i*i

        @tag_metadata(name="boop")
        @property
        def meta2(self):
            return 12

    a = A()
