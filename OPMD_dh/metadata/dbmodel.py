from sqlalchemy.orm import declarative_base, relationship, backref, as_declarative

#NOTE: may need to be changed for sqlalchemy 2.0
from sqlalchemy.orm.decl_base import _declarative_constructor
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import reconstructor
from sqlalchemy import Column, Integer, String, ForeignKey, Table, Boolean, DateTime, Enum, JSON, Date, Float, event
from sqlalchemy_utils import EmailType
from typing import Optional, Callable, Mapping, TypeVar, Type
import pathlib
from datetime import datetime, timezone
import enum
import json
from sqlalchemy import create_engine
import uuid
from nacl.signing import SigningKey
from nacl.encoding import HexEncoder
from pathvalidate import sanitize_filepath
from sqlalchemy.inspection import inspect
import sqlalchemy as sa
import logging
import zoneinfo
from ..operator_utils import OperatorManager
import pint

logger = logging.getLogger("DH:dbmodel")
ItemT = TypeVar("ItemT", bound="BaseModel")

class __BaseModel:
    def __init__(self, **kwargs):
        self._common_init()
        #self.logger.debug("declared logging class in BaseModel")
        _declarative_constructor(self, **kwargs)

    @reconstructor
    def init_on_load(self):
        self._common_init()

    def _common_init(self):
        self.logger = logging.getLogger(type(self).__name__)

BaseModel = declarative_base(cls=__BaseModel, constructor=None)


def _deserialize_quantities(dctin: dict, ureg=None) -> dict:
    outdct = {}
    if ureg is None:
        ureg = pint.get_application_registry()
    for k,v in dctin.items():
        if isinstance(v, Mapping) and "unit" in v and "value" in v:
            outdct[k] =  ureg.Quantity(value=v["value"], units=v["unit"])
        else:
            outdct[k] = v
            
    return outdct

def _serialize_quantities(dctin: dict) -> dict:
    outdct = {}
    for k,v in dctin.items():
        if isinstance(v, pint.Quantity):
            outdct[k] = {"unit" : str(v.units), "value" : v.magnitude}
        else:
            outdct[k] = v

    return outdct
    



class ModelMixin:
    upd_local = Column(Boolean, default=False, onupdate=True)
    
    def get_dumpfilepath(self, dhrepo, *args, **kwargs) -> pathlib.Path:

        schematp = self.__marshmallow__
        dumppath = schematp.get_classdumpfilepath(dhrepo, raise_exc=False)
        if dumppath is None:
            if fmtstr := getattr(schematp, "FILENAME_FMT", False):
                print("filename format path")
                dumppath = pathlib.Path(fmtstr.format(repo=dhrepo, self=self))
                self.logger.debug("computed dump path: {dumppath}")
            else:    
                self.logger.error(f"don't know how to dump object of type {type(self)}")
                raise RuntimeError("cannot determine how to name file for schema object dump")
        print(f"dumppath: {dumppath}")
        return dumppath

    @classmethod
    def load(cls: Type[ItemT], session, lookup_kw: Optional[str] = None, fail_on_missing: bool = True,
             **kwargs) -> Optional[ItemT]:
        
        logger.debug(f"requested load for {cls.__name__} object")
        if lookup_kw is None:
            logger.debug("looking up with primary key")
            pkeys = inspect(cls).primary_key
            if len(pkeys) > 1:
                raise NotImplemented("can't load_or_create default function for a class with more than one primary key!")
            lookup_kw = pkeys[0].name
            lookup_col = pkeys[0]
        else:
            lookup_col = inspect(cls).columns[lookup_kw]

            #NOTE: this should just fail on .scalar_one_or_none() if the item itself isn't unique, no need
            #to enforce at the column level we think...
            #if not lookup_col.unique:
            #    raise RuntimeError(f"lookup column {lookup_col.name} is not set as unique in the mapper!")

        logger.debug(f"looking up with lookup_kw: {lookup_kw}")
        if lookup_kw not in kwargs:
            raise KeyError(f"lookup key {lookup_kw} not found in arguments. Arguments were: {kwargs}")

        stmt = sa.select(cls).where(lookup_col==kwargs[lookup_kw])
        obj = session.execute(stmt).scalar_one_or_none()

        if fail_on_missing and obj is None:
            raise RuntimeError(f"failed to lookup object of type {cls.__name__} with lookup_kw: {lookup_kw} = {kwargs[lookup_kw]}")

        return obj
        
    
    @classmethod
    def load_or_create(cls: Type[ItemT], session, lookup_kw: Optional[str] = None,
                       overwrite:Optional[bool] = False, **kwargs) -> ItemT:

        logger.debug(f"requested load_or_create for {cls.__name__} object")

        obj = cls.load(session, lookup_kw, fail_on_missing=False, **kwargs)
        
        if obj is None:
            logger.debug("no object exists, creating new one")
            #create new object
            newobj = cls(**kwargs)
            session.add(newobj)
            session.commit()
            return newobj
        elif overwrite:
            newobj = cls(**kwargs)
            mobj = session.merge(newobj)
            return mobj
        else:
            return obj

        
class Operator(BaseModel, ModelMixin):
    __tablename__ = "operators"
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    email = Column(EmailType, nullable=False, unique=True)
    institution = Column(String, nullable=True)

    @classmethod
    def generate_signing_keys(cls):
        signing_key = SigningKey.generate()
        verify_key = signing_key.verify_key
        skeyhex = signing_key.encode(encoder=HexEncoder)
        vkeyhex = verify_key.encode(encoder=HexEncoder)
        return skeyhex, vkeyhex

component_type_procedure_type_association = Table(
    "component_type_procedure_type_assoc",
    BaseModel.metadata,
    Column("comptype", ForeignKey("component_types.id")),
    Column("proctype", ForeignKey("procedure_types.oneword_name"))
    )

component_procedure_association = Table(
    "component_procedure_assoc",
    BaseModel.metadata,
    Column("comp", ForeignKey("components.project_serial_number")),
    Column("procedure", ForeignKey("procedures.id")))


class ComponentType(BaseModel, ModelMixin):
    """Represents a physical component (e.g. DUT, tool or other things"""
    __tablename__: str = "component_types"

    id = Column(Integer, primary_key=True)
    descriptive_name = Column(String, nullable=False, unique=True)
    manufacturer = Column(String, nullable=False)
    manufacturer_part_number = Column(String, nullable=True)

    supplier = Column(String, nullable=True)
    supplier_part_number = Column(String,nullable=True)
    procedure_types = relationship("ProcedureType",
                                   secondary=component_type_procedure_type_association,
                                   back_populates="component_types")

class Location(BaseModel, ModelMixin):
    __tablename__ = "locations"
    id = Column(Integer, primary_key=True)
    descriptive_name = Column(String, nullable=False, unique=True)
    lat = Column(Float)
    lon = Column(Float)

class Component(BaseModel, ModelMixin):
    """Represents a specific instance of a physical component"""
    __tablename__ = "components"
    project_serial_number = Column(String, primary_key=True,
                                   autoincrement=False, unique=True)
    manufacturer_serial_number = Column(String, unique=True)
    typeid = Column(Integer, ForeignKey("component_types.id"), nullable=False)
    type = relationship("ComponentType", backref="instances")
    notes = Column(String)
    procedures = relationship("Procedure", secondary=component_procedure_association, back_populates="components")
    location_id = Column(Integer, ForeignKey("locations.id"))
    current_location = relationship("Location", backref="components")
    parent_id = Column(Integer, ForeignKey("components.project_serial_number"))
    children = relationship("Component", backref=backref("parent",remote_side=[project_serial_number]))
    entered_by_id = Column(Integer, ForeignKey("operators.id"), nullable=False)
    entered_by = relationship("Operator", backref="components")

class TravelerEntryTypes(enum.Enum):
    ACQUISITION = 0
    PROCEDURE_RUN = 1
    SHIPPING = 2
    ASSEMBLY = 3
    OTHER = 4

class TravelerEntry(BaseModel, ModelMixin):
    __tablename__ = "traveler_entries"
    id = Column(Integer,primary_key=True)
    recorded_at = Column(DateTime, nullable=False, default=datetime.utcnow().replace(tzinfo=timezone.utc))
    notes = Column(String)
    traveler_metadata = Column(JSON)
    component_id = Column(ForeignKey("components.project_serial_number"), nullable=False)
    component = relationship("Component")
    operator_id = Column(ForeignKey("operators.id"), nullable=False)
    entered_by = relationship("Operator", backref="traveler_entries")
    procedure_id = Column(ForeignKey("procedures.id"), nullable=True)
    procedure = relationship("Procedure")
    entry_type = Column(Enum(TravelerEntryTypes), nullable=False)

class ProcedureClass(enum.Enum):
    EXPERIMENT = 0
    DERIVED = 1
    CALIBRATION = 2
    OTHER = 3

pt_pt_assoc = Table("pt_pt_assoc", BaseModel.metadata,
                    Column("upstream", ForeignKey("procedure_types.oneword_name")),
                    Column("downstream", ForeignKey("procedure_types.oneword_name")))

pt_param_assoc = Table("pt_param_assoc", BaseModel.metadata,
                       Column("proceduretype", ForeignKey("procedure_types.oneword_name")),
                       Column("parameters", ForeignKey("parameter_defs.oneword_name")))

class ProcedureType(BaseModel, ModelMixin):
    __tablename__ = "procedure_types"
    friendly_name = Column(String, nullable=False)
    oneword_name = Column(String, primary_key=True, unique=True)
    notes = Column(String)
    component_types = relationship("ComponentType",
                                   secondary=component_type_procedure_type_association, back_populates="procedure_types")
    procclass = Column(Enum(ProcedureClass))
    base_file_path = Column(String, nullable=False)
    module_name = Column(String)
    class_name = Column(String)
    code_file_name = Column(String)
    upstream_procedure_types = relationship("ProcedureType",
                                            secondary=pt_pt_assoc,
                                            backref="downstream_procedure_types",
                                            primaryjoin=oneword_name==pt_pt_assoc.c.downstream,
                                            secondaryjoin=oneword_name==pt_pt_assoc.c.upstream)

    parameters = relationship("ParameterDef", secondary=pt_param_assoc,
                              backref="proceduretype",
                              primaryjoin=oneword_name==pt_param_assoc.c.proceduretype,
                              secondaryjoin=oneword_name==pt_param_assoc.c.parameters)



    @classmethod
    def from_python_class(cls, procclass):
        assert hasattr(procclass, "FRIENDLY_NAME")
        notes: str = getattr(procclass, "NOTES", "")
        return cls(friendly_name=procclass.FRIENDLY_NAME,
                   oneword_name=procclass.ONEWORD_NAME,
                   base_file_path = procclass.BASE_PATH_FORMAT,
                   module_name = procclass.__module__,
                   class_name = procclass.__qualname__)


class ExperimentStatus(enum.Enum):
    TODO = 0
    RUNNING = 1
    COMPLETE = 2
    ERROR = 3

proc_proc_assoc = Table("proc_proc_assoc",
                        BaseModel.metadata,
                        Column("upstream", ForeignKey("procedures.id")),
                        Column("downstream", ForeignKey("procedures.id")))



class Procedure(BaseModel, ModelMixin):
    __tablename__ = "procedures"
    id = Column(Integer, primary_key=True, unique=True)
    uuid = Column(String, unique=True,
                  default=lambda: uuid.uuid4().hex)
    operator_id = Column(Integer, ForeignKey("operators.id"))
    operator = relationship("Operator", backref="procedures")
    notes = Column(String)
    code_git_dirty = Column(Boolean)
    code_git_commit = Column(String)
    components = relationship("Component", secondary = component_procedure_association, back_populates="procedures")
    type_name = Column(String, ForeignKey("procedure_types.class_name"), nullable=False)
    type = relationship("ProcedureType", backref="procedures")
    device_under_test_id = Column(String, ForeignKey("components.project_serial_number"), nullable=False)
    device_under_test = relationship("Component", backref="procedures_DUT")
    create_time = Column(DateTime, nullable=False, default=datetime.utcnow().replace(tzinfo=timezone.utc))
    start_time = Column(DateTime)
    end_time = Column(DateTime)
    status = Column(Enum(ExperimentStatus), nullable=False, default=ExperimentStatus.TODO)
    dset_metadata = Column(JSON)
    upstream_procedures = relationship("Procedure", secondary=proc_proc_assoc, backref="downstream_procedures",
                                       primaryjoin=id==proc_proc_assoc.c.downstream,
                                       secondaryjoin=id==proc_proc_assoc.c.upstream)
    artifact_path = Column(String)
    intake_catalog_path=Column(String)
    status_info = Column(JSON)

    _parameter_values = Column("parameter_values", JSON)
    steps = relationship("ProcedureStep", back_populates="procedure")


    @hybrid_property
    def nextstepseq(self) -> int:
        if self.steps is None:
            return 0
        else:
            return len(self.steps)


    def __init__(self, dhtool, **kwargs):
        self._ureg = pint.get_application_registry()
        super().__init__( **kwargs)
        self._seqnum: int = 0

        self.logger.debug(f"uuid is: {self.uuid}")
        if dhtool is not None:
            self.logger.debug("setting artifact path...")
            rel_afact_path = self.get_artifact_path(dhtool)
            self.artifact_path = str(rel_afact_path)

            if "operator" not in kwargs and "operator_id" not in kwargs:
                opid = OperatorManager.get_operator_id(dhtool)
                self.operator_id = opid


    @property
    def short_uuid(self):
        return self.uuid[:8]

    @property
    def parameter_values(self) -> dict:
        return _deserialize_quantities(self._parameter_values, self._ureg)

    @parameter_values.setter
    def parameter_values(self, dct) -> None:
        self._parameter_values = _serialize_quantities(dct)


    @property
    def metadata_dict(self):
        if self.dset_metadata is None:
            return {}
        else:
            return json.loads(self.dset_metadata)
        

    def get_artifact_path(self, dhrepo) -> pathlib.Path:
        basedir = self.get_basedir(dhrepo)
        self.logger.debug(f"basedir: {basedir}")
        
        rel =  basedir.relative_to(dhrepo.basepath)
        self.logger.debug(f"rel: {rel}")
        return rel
    
    def get_basedir(self, dhrepo)  -> pathlib.Path:
        #TODO: may need more functionality to be used without a DH repo

        pattern = self.type.base_file_path
        self.logger.debug(f"pattern: {pattern}")
        pathstr = pattern.format(procedure=self)

        pth =  pathlib.Path(pathstr)

        self.logger.debug(f" formatted path pattern {pathstr}")
        if pth.is_absolute():
            raise ValueError(f"path {pth} is  absolute. Should be relative! Bad definition in ProcedureType")
        basedir = dhrepo.basepath / pth
        return basedir


    def get_dumpfilepath(self, dhrepo):
        basedir = self.get_basedir(dhrepo)
        return basedir /  "dh_meta/procedure.yml"

    def suggest_new_artifact_filename(self,  afact_type, seqnum: Optional[int]=None,
                                      step: Optional = None,
                                      mdat: Optional[dict] = None, dt: Optional[datetime] = None):
        
        fmtstr = afact_type.file_path_format
        if seqnum is None:
            seqnum = self._seqnum
            self._seqnum +=1
        if mdat is None:
            mdat = {}
        if dt is None:
            dt = datetime.now()

        self.logger.debug(f"step plan data: {step.plan_data}")
        newfname = fmtstr.format(procedure=self, afact_type=afact_type, seqnum=seqnum,
                                 mdat=mdat, dt=dt, step=step)
        return sanitize_filepath(newfname)

#event listener to make sure UUID is setup

@event.listens_for(Procedure, "init")
def uuid_init(target, args, kwargs):
    target.uuid = uuid.uuid4().hex



    

class ProcedureStep(BaseModel, ModelMixin):
    __tablename__ = "procedure_steps"
    id = Column(Integer, primary_key=True)
    procstepseq = Column(Integer, nullable=False)
    starttime = Column(DateTime, nullable=True)
    endtime = Column(DateTime, nullable=True)
    status = Column(Enum(ExperimentStatus), nullable=False,
                    default=ExperimentStatus.TODO)
    _plan_data = Column("plan_data", JSON)
    procedure_id = Column(Integer, ForeignKey("procedures.id"), nullable=False)
    procedure = relationship("Procedure", back_populates="steps")
    

    def __init__(self, *, procedure, ureg=None, **kwargs):
        self.procstepseq = procedure.nextstepseq

        if ureg is None:
            self._ureg = pint.get_application_registry()

        super().__init__(procedure=procedure, **kwargs)

    def get_dumpfilepath(self, dhrepo):
        procdir = self.procedure.get_basedir(dhrepo)
        return procdir/ "dh_meta/steps.yml"

    @property
    def plan_data(self) -> dict:
        return _deserialize_quantities(self._plan_data)

    @plan_data.setter
    def plan_data(self, dctin: dict) -> None:
        self._plan_data = _serialize_quantities(dctin)


class ArtifactType(BaseModel, ModelMixin):
    __tablename__ = "artifact_types"
    name = Column(String, unique=True, primary_key=True)
    file_path_format = Column(String, nullable=False)
    comment = Column(String, nullable=True)

    @classmethod
    def from_python_class(cls, afactclass):
        assert hasattr(afactclass, "ARTIFACT_TYPE_NAME")
        return cls(name=getattr(afactclass, "ARTIFACT_TYPE_NAME"),
                   file_path_format=getattr(afactclass, "FILE_PATH_FORMAT"))


class Artifact(BaseModel, ModelMixin):
    __tablename__ = "artifacts"
    path = Column(String, unique=True, primary_key=True)
#    procedure_id = Column(Integer, ForeignKey("procedures.id"), nullable=False)
    step_id = Column(Integer, ForeignKey("procedure_steps.id"), nullable=False)
#    procedure = relationship("Procedure", backref="artifacts")
    step = relationship("ProcedureStep", backref="artifacts")
    afact_type = relationship("ArtifactType")
    afact_type_id = Column(String, ForeignKey("artifact_types.name"), nullable=True)
    _afact_metadata = Column(JSON)

    def __init__(self, **kwargs):
        self._ureg = pint.get_application_registry()
        super().__init__(**kwargs)

    def get_dumpfilepath(self, dhrepo):
        procdir = self.step.procedure.get_basedir(dhrepo)
        return procdir / "dh_meta/artifacts.yml"

    @property
    def afact_metadata(self) -> dict:
        return _deserialize_quantities(self._afact_metadata, self._ureg)
    
    @afact_metadata.setter
    def afact_metadata(self, dct) -> None:
        self._afact_metadata = _serialize_quantities(dct)
        
        
class ParameterDef(BaseModel, ModelMixin):
    __tablename__ = "parameter_defs"                  
    oneword_name = Column(String, unique=True, primary_key=True)
    notes = Column(String, nullable=True)
    unit = Column(String, nullable=True)
    pytype = Column(String, nullable=True)


class DHMetaKV(BaseModel):
    __tablename__ = "dh_meta_kv"
    key = Column(String, unique=True, primary_key=True)
    value = Column(JSON)


if __name__ == "__main__":
    engine = create_engine("sqlite:///:memory:", echo=True)
    BaseModel.metadata.create_all(engine)
