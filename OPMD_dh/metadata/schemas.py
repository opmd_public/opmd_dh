from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, auto_field
from marshmallow_enum import EnumField
from .dbmodel import Operator, ComponentType, Location, Component, TravelerEntry
from .dbmodel import ProcedureType, ProcedureClass, Procedure, Artifact, ArtifactType, TravelerEntryTypes
from .dbmodel import ProcedureStep, ParameterDef, ExperimentStatus

from marshmallow import fields
from slugify import slugify
import os
from typing import Optional, Callable
import pathlib

class BaseAutoSchema(SQLAlchemyAutoSchema):
    def __init_subclass__(cls,  **kwargs):
        super().__init_subclass__(**kwargs)
        if (metadef := getattr(cls,"Meta", None)) is not None:
            cls.__sqlalchemy__ = metadef.model
            metadef.model.__marshmallow__ = cls

    class Meta:
        load_instance: bool = True
        include_fk: bool = True
        ordered: bool = True

    @classmethod
    def _get_default_dumpfile(cls, basepath: Optional[pathlib.Path] = None):
        clsname =  slugify(cls.Meta.model.__name__)
        flname = clsname + ".yml"
        if basepath is None:
            return flname
        else:
            return basepath / flname

    @classmethod
    def get_classdumpfilepath(cls, dhrepo, raise_exc: bool=True) -> Optional[pathlib.Path]:
        if getattr(cls, "STANDALONE_SCHEMA_FILE", False):
            print("standalone schemas path...")
            print(dhrepo.standalone_schemas_path)
            return cls._get_default_dumpfile(dhrepo.standalone_schemas_path)

        if raise_exc:
            raise TypeError(f"tried to use get_classdumpfilepath on class {cls} that doesn't support it!")
        return None

class OperatorSchema(BaseAutoSchema):
    STANDALONE_SCHEMA_FILE: bool = True
    class Meta(BaseAutoSchema.Meta):
        model = Operator

    id = auto_field(metadata={"interactive_create" : False})
    name = auto_field(metadata={"interactive_order" : 0})
    email = auto_field(metadata={"interactive_order" : 1})

class ComponentTypeSchema(BaseAutoSchema):
    STANDALONE_SCHEMA_FILE: bool = True
    class Meta(BaseAutoSchema.Meta):
        model = ComponentType

    id = auto_field(metadata={"interactive_create" : False})
    descriptive_name = auto_field(metadata={"interactive_order" : 0})
    
class LocationSchema(BaseAutoSchema):
    STANDALONE_SCHEMA_FILE: bool = True
    class Meta(BaseAutoSchema.Meta):
        model = Location

class ComponentSchema(BaseAutoSchema):
    FILENAME_FMT: str = "{repo.meta_base_path}/components/{self.type.manufacturer_part_number}/{self.project_serial_number}.yml"
    class Meta(BaseAutoSchema.Meta):
        model = Component

class TravelerEntrySchema(BaseAutoSchema):
    FILENAME_FMT: str = "{repo.meta_base_path}/travelers/{self.component_id}.yml"
    MULTIPLE_ENTRIES_PER_FILE: bool = True
    class Meta(BaseAutoSchema.Meta):
        model = TravelerEntry
    entry_type = EnumField(TravelerEntryTypes)

class ProcedureTypeSchema(BaseAutoSchema):
    FILENAME_FMT: str = "{repo.meta_base_path}/procedure_types/{self.oneword_name}.yml"
    class Meta(BaseAutoSchema.Meta):
        model = ProcedureType
    procclass = EnumField(ProcedureClass)


class ProcedureSchema(BaseAutoSchema):
    class Meta(BaseAutoSchema.Meta):
        model = Procedure
        include_relationships = True
    status = EnumField(ExperimentStatus)

class ArtifactSchema(BaseAutoSchema):
    MULTIPLE_ENTRIES_PER_FILE: bool = True
    class Meta(BaseAutoSchema.Meta):
        model = Artifact

class ArtifactTypeSchema(BaseAutoSchema):
    FILENAME_FMT: str = "{repo.meta_base_path}/artifact_types/{self.name}.yml"
    class Meta(BaseAutoSchema.Meta):
        model = ArtifactType
    
class ProcedureStepSchema(BaseAutoSchema):
    MULTIPLE_ENTRIES_PER_FILE: bool = True
    class Meta(BaseAutoSchema.Meta):
        model = ProcedureStep
    status = EnumField(ExperimentStatus)
        
class ParameterDefSchema(BaseAutoSchema):
    STANDALONE_SCHEMA_FILE: bool = True
    class Meta(BaseAutoSchema.Meta):
        model = ParameterDef

