from intake.source.base import DataSourceBase, Schema
from fsspec import open
from OPMD_dh.file_formats.base import OPMDDataTypes

class OPMDH5SourceMixin:
    def __init__(self, flpath, metadata=None, storage_options):
        super().__init__(metadata=metadata)
        self._flpath = flpath
        self._storage_options = storage_options

    def _get_schema(self):
        from OPMD_dh.file_formats.hdf5 import H5OPMDFile
        #it is recommended in intake docs to use fsspec
        fobj = open(self._flpath, **self._storage_options)
        self._fl = H5OPMDFile.load(fobj)

        tgtdtype = getattr(self, "_OPMD_DATA_TYPE", None)
        if tgtdtype is not None:
            assert tgtdtype in self._fl.held_datatypes()
            grp = self._fl[self._H5_BASEPATH]
        else:
            grp = self._fl

        npart = self._fl.count_nonmeta_subgroups(grp)
        extra_mdat = self._fl.extract_hdf_metadata(grp)

            
        
        
        
        
        
    



class OPMDH5ImageSource(OPMDH5SourceMixin, DataSourceBase):
    name = "OPMDH5ImageSource"
    version = "1"
    partition_access=True
    container = "ndarray"
    _OPMD_DATA_TYPE = OPMDDataTypes.ARRAY
    _H5_BASEPATH = "arrays"
    
    def _get_partition(self, i):
        pass


class OPMDH5TableSource(DataSourceBase):
    name = "OPMDH5TableSource"
    version = "1"
    partition_access=True
    container = "dataframe"
    _OPMD_DATA_TYPE = OPMDDataTypes.TABLE
    _h5_BASEPATH = "tables"

    def _count_partitions(self):
        return self._fl.count_nonmeta_subgroups(self._fl["arrays"])


class OPMDH5MultiSource(DataSourceBase):
    name = "OPMDH5MultiSource"
    version=  "1"
    partition_access=False
    container = "python"
