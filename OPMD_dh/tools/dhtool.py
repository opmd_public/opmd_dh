#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 17 17:31:07 2022

@author: danw
"""

import pathlib
from typing import Union, Optional, Type
import click
import sys
import pwd
import os
from datalad.api import create
from ..datahandler import DHTool
from ..helpers import find_datalad_repo_root, NoDHRepoFoundError
from ..operator_utils import OperatorManager, ConfigManager
from ..metadata.dbmodel import BaseModel, Operator, Location
from ..metadata.schemas import OperatorSchema, LocationSchema
from sqlalchemy import create_engine, select
from sqlalchemy.orm import sessionmaker
import platform
import pwd
import prettytable
import logging






def get_logged_in_user_name() -> Optional[str]:
    if os.name != "posix":
        #TODO: implement at least for Windows, or maybe note
        return None

    pwstruct = pwd.getpwuid(os.geteuid())
    return pwstruct[4]

@click.command(name="registeroperator")
@click.argument("directory")
@click.option("--id", help="pre-select operator id", type=int)
def registeroperator(directory: str, id: Optional[int]):
    engine = create_engine("sqlite:///:memory:")
    BaseModel.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    tool = DHTool.from_path(directory, sqlalchemy_engine=engine,
                            sessiontp=Session)
    tool.load_all_schemas()

    with Session() as sess:
        if id is None:
            stmt = select(Operator).order_by(Operator.name)
            ops = sess.execute(stmt).scalars().all()
            tbl = prettytable.PrettyTable()
            tbl.field_names = ["id", "Name", "Institution"]

            for op in ops:
                tbl.add_row([op.id, op.name, op.institution])

            print(tbl)
            id = click.prompt("choose your operator id from the table", type=int)

        opstmt = select(Operator).where(Operator.id == id)
        opres = sess.execute(opstmt).scalars().one()

        if tool.shared_operators:
            print("setting global operator id in repo")
            OperatorManager.global_set_operator(tool, opres)
        else:
            print("setting local operator id in repo")
            OperatorManager.repo_set_operator(tool, opres)


@click.command(name="newoperator")
@click.argument("directory")
@click.option("--name", help="name of the operator", type=str,
                prompt="enter your name", default=get_logged_in_user_name())
@click.option("--institution", help="name of the institution", type=str,
              prompt="enter your institution", default="University of Oxford")
@click.option("--email", help="your email address", type=str,
              prompt="enter your email address")
def newoperator(directory: str, name: str, institution: str, email: str):
    engine = create_engine("sqlite:///:memory:")
    BaseModel.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    tool = DHTool.from_path(directory, sqlalchemy_engine=engine,
                            sessiontp=Session)

    tool.load_all_schemas()

    #check if the operator is already in there
    stmt = select(Operator).where(Operator.email == email)

    with Session() as sess:
        n_existing_ops = len(sess.execute(stmt).all())
        if n_existing_ops > 0:
            raise ValueError("existing operator with that email already found!")

        newop = Operator(name=name, institution=institution,
                         email=email)
        sess.add(newop)
        sess.commit()
        sess.refresh(newop)
        #save the operator details into the repo
        tool.dump_all_schemas(session=sess)

        #now save it into the system somewhere
        if tool.shared_operators:
            print("repo is shared, setting operator in the user's config directory")
            OperatorManager.global_set_operator(tool, newop)
        else:
            print("repo is not shared, setting operator in the repo local config")
            OperatorManager.repo_set_operator(tool, newop)


@click.command(name="init")
@click.argument("directory")
@click.option("--description", help="the description of the repository")
@click.option("--create-directory", is_flag=True,
              help="create the directory for the repo if it doesn't exist. Implies --create-repo")
@click.option("--create-repo", is_flag=True,
              help="create a datalad dataset for the repo if it doesn't exist")
@click.option("--shared", help="is this instance of the repo shared between users?")
def init_repo(directory: str, description: Optional[str], create_directory: Optional[bool],
              create_repo: Optional[bool], shared: Optional[bool]):
    pth = pathlib.Path(directory).expanduser().resolve()

    #first check if the path exists
    if not pth.exists():
        print("repo directory doesn't exist!")
        if create_directory or click.confirm("would you like to create a new directory?"):
            print("creating repo directory...")
            pth.mkdir(parents=True)
            new_directory: bool = True
        else:
            print("chose not to create a directory. Can't continue!")
            sys.exit(-1)
    else:
        new_directory: bool = False

    try:
        dlad_dset = find_datalad_repo_root(pth)
        engine = create_engine("sqlite:///:memory:")
        dhtool = DHTool(dlad_dset, engine)
    except NoDHRepoFoundError:
        if new_directory or click.confirm("create a new datalad repo?"):
            print("making datalad repo...")
            description = click.prompt("enter repo description") if description is None else description
            shared = click.confirm("is this copy of the repo  shared between operators?") if shared is None else shared

            dlad_repo = create(pth,force=True)
            dlad_repo.repo.config.add("opmd-dh.shared-operators", value=str(int(shared)), scope="local")
        else:
            print("chose not to create datalad repo, cannot continue");
            sys.exit(-1)


@click.command(name="makedb")
@click.argument("repodir",  type=click.Path(file_okay=False, dir_okay=True, exists=True, resolve_path=True))
@click.argument("outfile", type=click.Path(file_okay=True, dir_okay=False, resolve_path=True))
@click.option("--overwrite","-w", is_flag=True, help="overwrite existing output db if found")
def makedb(repodir: str, outfile: str, overwrite: bool):
    """ create an SQlite database (path provided as OUTFILE argument) from a DH repo (path provided as REPODIR argument)"""
    pth = pathlib.Path(repodir).expanduser().resolve()

    if outfile is not None:
        outpth = pathlib.Path(outfile).expanduser().resolve()
    try:
        dlad_dset = find_datalad_repo_root(pth)
    except NoDHRepoFoundError:
        print(f"couldn't find DH repo at specified path {pth}")
        sys.exit(-1)

    if outpth.exists() and not overwrite:
        print(f"not overwriting existing db file at {outpth}")
        sys.exit(-1)

    engine = create_engine(f"sqlite:///{outpth}")
    dhtool = DHTool(dlad_dset, engine)

    BaseModel.metadata.create_all(engine)
    dhtool.load_all_schemas()

    dhtool.linked_dbs = dhtool.linked_dbs.append(str(outpath))

@click.command(name="setdefaultrepo")
@click.argument("repopath", type=click.Path(file_okay=False, dir_okay=True, exists=True, resolve_path=True))
def setdefaultrepo(repopath):
    """sets a DH repo as the default one (to be used by OPMD_dh and running acquisition scripts etc)"""
    try:
        dhtool = DHTool.from_path(repopath)
    except NoDHRepoFoundError:
        print(f"no vaild DH repo found at path {repopath}, cannot continue")
        sys.exit(-1)

    ConfigManager.set_default_repo(repopath)



@click.command(name="ingestdb")
@click.argument("infile", type=click.Path(file_okay=True, dir_okay=False, exists=True, resolve_path=True))
@click.argument("repodir", type=click.Path(file_okay=False, dir_okay=True, exists=True, resolve_path=True))
@click.option("-y","--noconfirm", is_flag=True, help="do not do interactive confirmations")
def ingestdb(infile: str, repodir: str, noconfirm:bool):
    """ ingest an SQlite database file (which was created e.g. by dhtool makedb) provided at INFILE into a DH repo (supplied at REPODIR)"""

    logging.getLogger().setLevel(logging.DEBUG)
    
    repopth = pathlib.Path(repodir).expanduser().resolve()
    filepth = pathlib.Path(infile).expanduser().resolve()

    try:
        dlad_dset = find_datalad_repo_root(repopth)
    except NoDHRepoFoundError:
        print(f"couldn't find DH repo at specified path {repopth}")
        sys.exit(-1)

    engine = create_engine(f"sqlite:///{filepth}")
    dhtool = DHTool(dlad_dset, engine)

    if noconfirm or click.confirm("this operation will OVERWRITE files in the repo. Please check you've committed all wanted changes first... Proceed?"):
        dhtool.dump_all_schemas()
        
@click.group("dhtool", commands={"init" : init_repo,
                                 "newoperator": newoperator,
                                 "registeroperator" : registeroperator,
                                 "makedb" : makedb,
                                 "ingestdb" : ingestdb,
                                 "setdefaultrepo" : setdefaultrepo})
def dhtool():
    pass


if __name__ == "__main__":
    dhtool()
