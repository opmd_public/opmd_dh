
from .helpers import YamlFileProxy
import appdirs
from ruamel.yaml import YAML
import pathlib
from sqlalchemy import select
import warnings


class ConfigManager:
    CFG_SECTION="opmd-dh"

    @classmethod
    def _conf_file_dir(cls):
        conf_dir = pathlib.Path(appdirs.user_config_dir("OPMD_dh"))
        if not conf_dir.exists():
            conf_dir.mkdir(parents=True)
        return conf_dir

    @classmethod
    def set_default_repo(cls, repopath):
        if not isinstance(repopath, pathlib.Path):
            repopath = pathlib.Path(repopath).expanduser().resolve()

        conffile = cls._conf_file_dir() / "config.yml"
        with YamlFileProxy(conffile, writeable=True, typ="rt") as cfg:
            print(type(cfg))
            if "DEFAULT_DH_REPO" not in cfg:
                cfg["DEFAULT_DH_REPO"] = str(repopath)
            else:
                cfg["DEFAULT_DH_REPO"] = str(repopath)

    @classmethod
    def get_default_repo(cls):
        conffile = cls._conf_file_dir() / "config.yml"
        with YamlFileProxy(conffile, writeable=False, typ="rt") as cfg:
            if "DEFAULT_DH_REPO" in cfg:
                return cfg["DEFAULT_DH_REPO"]
            else:
                return None
        return opt


class OperatorManager(ConfigManager):
    CFG_SECTION="opmd-dh"
    CFG_OPTION="operator-id"
    CFG_ENTRY = f"{CFG_SECTION}.{CFG_OPTION}"
    
    @classmethod
    def repo_set_operator(cls, repo, op: "Operator"):
        if repo.shared_operators:
            raise RuntimeError("can't set operator in a shared_operators repo")
        if repo._repo.config.has_option(cls.CFG_SECTION, cls.CFG_OPTION):
            repo._repo.config.set(cls.CFG_ENTRY, value=str(op.id), scope="local")
        else:
            repo._repo.config.add(cls.CFG_ENTRY, value=str(op.id), scope="local")


    @classmethod
    def _operator_conf_file(cls):
        conf_dir = cls._conf_file_dir()
        operators_conf = conf_dir / "operator_conf.yml"
        return operators_conf

    @classmethod
    def global_set_operator(cls, repo, op: "Operator"):
        operators_conf = cls._operator_conf_file()
        print(operators_conf)
        with YamlFileProxy(operators_conf, writeable=True, typ="rt") as cfg:
            cfg[str(repo.basepath)] = op.id
            print(cfg)

    @classmethod
    def get_operator_id(cls, repo: "DHTool") -> int:
        if repo.shared_operators:
            operators_conf = cls._operator_conf_file()
            with YamlFileProxy(operators_conf, writeable=False, typ="rt") as cfg:
                opid = cfg[str(repo.basepath)]
                return int(opid)
        else:
            opid = int(repo._repo.config.get(cls.CFG_ENTRY))
            if opid is None:
                raise ValueError("no operator_id set in this repo!")
            return opid


    @classmethod
    def get_operator_obj(cls, repo: "DHTool") -> "Operator":
        with repo.Session() as sess:
            opid = cls.get_operator_id(repo)
            from OPMD_dh.metadata.dbmodel import Operator
            stmt = select(Operator).where(Operator.id == opid)
            op = sess.execute(stmt).scalar_one()
        return op
