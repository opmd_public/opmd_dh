from .metadata.dbmodel import ArtifactType
from .class_interface_utils import required_attributes, dbrep_generator
from .file_formats.hdf5 import H5OPMDFile

class ArtifactBase:
    def __init_subclass__(cls):
        required_attributes(cls, ["STORAGE_CLASS", "ARTIFACT_TYPE_NAME", "FILE_PATH_FORMAT"])

    db_artifacttype_rep = classmethod(dbrep_generator(ArtifactType,
                                                      lambda c : ArtifactType.name == c.ARTIFACT_TYPE_NAME))



