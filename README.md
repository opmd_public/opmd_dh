# OPMD_dh

**OPMD_dh** (unimaginatively, "OPMD Data Handling") is a library and set of
command line tools intended to help with the management of detector test data.

It was originally developed from a fork of our internal LSST focal plane
sensor test software, and is now being used in our test campaigns for the
MAGIS-100 primary cameras. 

It is intended to be able to be used without much institutional infrastructure
(in particular, no centralised database server is needed), though at the
moment it is quite minimal and the learning curve is a bit steep. 

Documentation is (slowly) in progress.

## Principles
The idea is that the test data themselves (e.g. images, spectra, etc), are
stored in whatever format you like (in practice we mostly use HDF5 for images
and tables) on disk, and checked into a
[git-annex](https://git-annex.branchable.com/) repository (though we actually
use the wrapper around this called `datalad` which is a little bit friendlier
and in wide use e.g. in the neuroscience community). Those data can sit in any
backend git-annex supports (local filesystem, filesystem accessible via rsync,
amazon S3-bucket compatible object store etc etc, many options). Meanwhile,
"high level" metadata (which means things like "what kind of experiment is
this, what were the top level parameters used to run the experiment, who ran
it, when") etc, are stored in YAML files in the normal `git` part of the
`git-annex` repository. 

Thus, someone can checkout an `OPMD_dh` repository, and they get a relatively
small download of the metadata. They can then query this metadata in order to
find the actual datasets they want, and then do a `git-annex get` command
which will download the data they want. In this way we can store very large
amounts of data in the same repo without burdening everyone who wants to look
at a couple of results with huge multi-TB downloads.

The second idea behind `OPMD_dh` is that on their own, metadata in YAML format
are not all that useful. So, we also implement a database schema which is
fairly general but tailored towards detector testing. `OPMD_dh` is able to
construct a database (currently SQLite only is supported but we use
`SQLAlchemy` and there's no reason in principle why other backends it
supports, notably `postgresql` couldn't be used), from the YAML files in the
repository. It is also able to update those YAML files from an instance of a
database.

So, one can maintain a database including metadata about experiments, and sync
it out to a standalone `git-annex` repository, and reproduce that database
from the repository. If a user wants to, they can just `grep` through a bunch
of YAML files to find what they want, or if they like they can construct the
database and use whatever querying tools they want to interrogate the metadata


